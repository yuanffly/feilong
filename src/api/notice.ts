import request from '@/utils/request'

export async function noticeList( data:any={} ) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`/reportservice/notice/notices`, option)
}


export async function noticeUpdate( data:any={} ) {
	const option = {
		method: 'patch',
		data,
		customError: true
	}
	return await request(`/reportservice/notice/notices`, option)
}

export async function toLookView( params:any={} ) {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(`/reportservice/chartGroup/charts/${params['chartGroupId']}`, option)
}

