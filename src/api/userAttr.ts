import request from '@/utils/request'


//用户属性
export async function userAttrList(params:any) {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(`reportservice/user-analysis/userAttr?projectId=${params['projectId']}`, option)
}
//比较值查询
export async function compareList(params:any) {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(`/reportservice/event/function?dataType=${params['dataType']}`, option)
}

//根据产品id获取查询维度
export async function userProperties(params:any) {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(`/reportservice/user-analysis/userProperties?projectId=${params['projectId']}`, option)
}

//根据id查询图表
export async function chartSource(params:any) {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(`/reportservice/chart/chartSource/${params['projectId']}`, option)
}

//根据id查询图表
export async function taskOne(params:any) {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(`/reportservice/task/${params['taskId']}`, option)
}

//属性值查询
export async function valueList(data = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`reportservice/event/attr-values`, option)
}

//用户属性计算
export async function userAttrCompute(data = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`reportservice/user-analysis`, option)
}

//保存报表
export async function saveResultCrowd(data = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`reportservice/userCrowd/saveResultCrowd`, option)
}

export async function saveReport(data = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`reportservice/chart`, option)
}

export async function chartGroup(data = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`reportservice/chart/chartGroup`, option)
}

//保存报表
export async function exportReport(data = {}) {
	const option = {
		method: 'put',
		data,
		customError: true
	}
	return await request(`reportservice/task`, option)
}
