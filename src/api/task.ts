import request from '@/utils/request'

export async function tasksList( data:any={} ) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`/reportservice/task/tasks`, option)
}

