import request from '@/utils/request'

/**
 * 获取用户信息
 */
export async function getAccountMation( ) {
	const option = {
		method: 'get',
		customError: true,
	}
	return await request(`/reportservice/account`, option)
}

/**
 * 修改账户密码
 */
export async function editAccountPassword( data:any={} ) {
	const option = {
		method: 'put',
		data,
		customError: true,
	}
	return await request(`/reportservice/account/rest/password`, option)
}

/**
 * 更新用户信息
 */
export async function updateAccountMation( data:any={} ) {
	const option = {
		method: 'patch',
		data,
		customError: true,
	}
	return await request(`/reportservice/account/rest/user`, option)
}

