import request from '@/utils/request'
/**
 * 获得用户属性
 */
export async function getUserAttr(projectId: string | number = '') {
	const option = {
		method: 'get',
		// data
		customError: true
	}
	return await request(`/reportservice/user-analysis/userAttr?projectId=${projectId}`, option)
}

/**
 * 获得事件列表
 */
export async function getEvents(projectId: string | number = '') {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(`/reportservice/event/eventGroups/?projectId=${projectId}`, option)
}

/**
 * 获得事件属性
 */
export async function getEventAttr(
	projectId: string | number = '',
	eventCode: string | number = ''
) {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(
		`/reportservice/event/filtProperties?projectId=${projectId}&eventCodes=${eventCode}`,
		option
	)
}

/***
 * 用户行为中 选择事件之后的下拉
 *
 */
export async function getEventProperties(
	projectId: string | number = '',
	eventCode: string | number = ''
) {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(
		`/reportservice/userCrowd/getEventProperties?projectId=${projectId}&eventCode=${eventCode}`,
		option
	)
}

/**
 * 获得逻辑符号,如：等于、大于、小于
 * dataType: 是属性的类型，有string\number\datetime\bool
 */
export async function getFunction(type: string | number = '') {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(`/reportservice/event/function?dataType=${type}`, option)
}

/**
 * 获得逻辑符号,如：等于、大于、小于
 * dataType: 是属性的类型，有string\number\datetime\bool
 */
export async function getUserFunction(type: string | number = '') {
	const option = {
		method: 'get',
		customError: true
		// data
	}
	return await request(`/reportservice/user-analysis/function?dataType=${type}`, option)
}

/**
 * 获得用户属性或事件属性对应的值
 */
export async function getAttrValues(data: any = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`/reportservice/event/attr-values`, option)
}

/**
 * 分群列表
 */
export async function getUserCrowdList(data: any = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`/reportservice/userCrowd/getUserCrowdInfoList`, option)
}

/**
 * 分群规则保存
 */
export async function userCrowdSave(data: any = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`/reportservice/userCrowd/update`, option)
}

/**
 * 列表删除
 */
export async function userCrowdDel(id: string | number) {
	const option = {
		method: 'post',
		data: {
			id,
			status: 2
		},
		customError: true
	}
	return await request(`/reportservice/userCrowd/update`, option)
}

/**
 * 单个分群详情
 */
export async function getUserCrowdInfo(id: string | number = '') {
	const option = {
		method: 'get',
		customError: true
	}
	return await request(`/reportservice/userCrowd/getUserCrowdInfo?userCrowdId=${id}`, option)
}

/**
 * 列表操作按钮下载
 */
export async function crowdDownload(crowdId = '', projectId = '') {
	const option = {
		method: 'get',
		customError: true
	}
	return await request(
		`/reportservice/userCrowd/download?crowdId=${crowdId}&projectId=${projectId}`,
		option
	)
}

/**
 * 列表操作更新
 */

export async function crowdCalc(data = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`/reportservice/userCrowd/calculation`, option)
}

/**
 * 上传id创建分群保存
 */

export async function uploadId(data = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`/reportservice/userCrowd/uploadIdCrowd`, option)
}

/**
 * 上传id创建分群中下载模板
 */

export async function downloadIdTemplate() {
	const option = {
		method: 'get',
		customError: true
	}
	return await request(`/reportservice/userCrowd/downloadIdTemplate`, option)
}

/**
 * lookalike保存
 */

export async function lookLikeCrowd(data = {}) {
	const option = {
		method: 'post',
		data,
		customError: true
	}
	return await request(`/reportservice/userCrowd/lookLikeCrowd`, option)
}

/**
 * 种子人群列表
 */

export async function lookLikeCrowdList(project: string | number = '') {
	const option = {
		method: 'get',
		customError: true
	}
	return await request(
		`/reportservice/userCrowd/getConditionCrowdSelect?projectId=${project}`,
		option
	)
}

/**
 * 种子人群模板下载
 */
export async function downloadLLTemplate() {
	const option = {
		method: 'get',
		customError: true
	}
	return await request(`/reportservice/userCrowd/downloadLLTemplate`, option)
}
