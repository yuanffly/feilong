import request from '@/utils/request'

/**
 *sql列表下载接口
 */
export async function sqlDownload( data:any={} ) {
	const option:any = {
		responseType:'blob',
		method: 'post',
		data,
		customError: true,
	}
	return await request(`/reportservice/sql/download`, option)
}
/**
 * 保存查询sql接口
 */
export async function saveSqlSentence( data:any={} ) {
	const option = {
		method: 'post',
		data,
		customError: true,
	}
	return await request(`/reportservice/sql/create`, option)
}

/**
 * 删除查询sql接口
 */
export async function deleteSqlSentence( data:any={} ) {
	const option = {
		method: 'post',
		data,
		customError: true,
	}
	return await request(`/reportservice/sql/delete`, option)
}

/**
 * sql列表接口
 */
export async function requestSqlList( data:any={} ) {
	const option = {
		method: 'post',
		data,
		customError: true,
	}
	return await request(`/reportservice/sql/list`, option)
}

/**
 * sql查询接口
 */
export async function querySqlSentence( data:any={} ) {
	const option = {
		method: 'post',
		data,
		customError: true,
	}
	return await request(`/reportservice/sql/query`, option)
}

/**
 * 表列表接口
 */
export async function querySqltables( data:any={} ) {
	const option = {
		method: 'post',
		data,
		customError: true,
	}
	return await request(`/reportservice/sql/tables`, option)
}




