// import {Vue} from "vue-property-decorator"

export default {
	userAttrs:{
		"code": 200,
		"msg": "成功",
		data: [{"attr_name": "用户ID", "system_code": "user_id", "db_type": "varchar"},//字段与中文名的关系 , 用作3.1传参时，传system_code
			{"attr_name": "城市", "system_code": "_city", "db_type": "varchar"},
			{"attr_name": "省份", "system_code": "_province", "db_type": "varchar"}]
	},




	/* data_type 为 bool 时选，没有 3 级信息 */
	boolOpts: [
		{ key: 'isTrue', name: '为真' },
		{ key: 'isFalse', name: '为假' },

		{ key: 'isSet', name: '有值' },
		{ key: 'notSet', name: '无值' },
	],

	/* data_type 为 string 时选 */
	textOpts: [
		{ key: 'equal', name: '等于' },             // 可输入多个字符串
		{ key: 'notEqual', name: '不等于' },

		{ key: 'contain', name: '包含' },
		{ key: 'notContain', name: '不包含' },

		{ key: 'isSet', name: '有值' },
		{ key: 'notSet', name: '无值' },
		{ key: 'isEmpty', name: '为空' },
		{ key: 'isNotEmpty', name: '不为空' },

		// { key: 'rlike', name: '正则匹配' },
		// { key: 'notrlike', name: '正则不匹配' },
	],

	/* data_type 为 number 时选 */
	numberOpts: [
		{ key: 'equal', name: '等于' },         // 可输入多个数字
		{ key: 'notEqual', name: '不等于' },

		{ key: 'less', name: '小于' },          // 只需输入一个数字
		{ key: 'greater', name: '大于' },

		{ key: 'between', name: '区间' },       // 需输入 2 个数字，并且

		{ key: 'isSet', name: '有值' },
		{ key: 'notSet', name: '无值' },
	],

	/* data_type 为 datetime 时选  */
	datetimeOpts: [
		{ key: 'absolute_between', name: '绝对时间' },    // 任意时间区间（精确到分，但是好像是写死 00:00）
		{ key: 'relative_within', name: '相对当前时间点', arr: [{ key: 'relative_within', name: '之内' }, { key: 'relative_before', name: '之前' }] },
		{ key: 'relative_between', name: '相对当前时间区间' },
		{ key: 'relative_event_time', name: '相对事件发生时间' },

		{ key: 'isSet', name: '有值' },
		{ key: 'notSet', name: '无值' },
	],

	/* data_type 为 list 时选 */
	listOpts: [
		{ key: 'contain', name: '包含' },
		{ key: 'notContain', name: '不包含' },

		{ key: 'isEmpty', name: '为空' },
		{ key: 'isNotEmpty', name: '不为空' },

		{ key: 'isSet', name: '有值' },
		{ key: 'notSet', name: '无值' },
	],

	// 相对事件发生时间 选项
	relativeEventTime: [
		{ key: '+', name: '(现在) 之后' },
		{ key: '-', name: '(现在) 之前' },
		{ key: 'day', name: '今天' },
		{ key: 'week', name: '本周' },
		{ key: 'month', name: '本月' },
	],

	// 时间单位
	timeUnit: [
		{ key: 'second', name: '秒' },
		{ key: 'minute', name: '分种' },
		{ key: 'hour', name: '小时' },
		{ key: 'day', name: '天' },
	],

	// 漏斗窗口期
	defaultFunnelWindowsTimeList: [
		{ label: "5分钟", value: "5" },
		{ label: "1小时", value: "60" },
		{ label: "当天", value: "-1" },
		{ label: "24小时", value: "1440" },
		{ label: "7天", value: "10080" },
		{ label: "14天", value: "20160" },
		{ label: "30天", value: "43200" },
		{ label: "60天", value: "86400" },
		{ label: "90天", value: "129600" },
		{ label: "180天", value: "259200" },
		{ label: "自定义", value: "自定义" }
	],

	//自定义漏斗窗口期单位
	defaultTimeUnit: [
		{label: '天'},
		{label: '小时'},
		{label: '分钟'}
	],

	// 自定义校验条件
	rules: {
		// 可输入多个值
		createCommon: {
			dataTypeArr: ['number', 'string'],      //第一个下拉框
			newDataTypeArr: ['equal', 'notEqual','contain','notContain'],  // equal 等于 | notEqual 不等于      第二个下拉框

			key: 'customArr',        //第三个下拉框
		},

		// 可输入单个值
		inputCommon: {
			dataTypeArr: ['number', 'string'],

			// less 小于 | greater 大于 | contain 包含 | notContain 不包含 | rlike 正则匹配 | notrlike 正则不匹配
			newDataTypeArr: ['less', 'greater', 'rlike', 'notrlike'],

			key: 'customStr',
		},

		// 数值区间
		numBetween: {
			dataTypeArr: ['number'],
			newDataTypeArr: ['between'],            // between 区间

			key: ['customNumBetween_0', 'customNumBetween_1'],
		},

		// 时间区间 默认
		autoDateBetween: {
			dataTypeArr: ['datetime'],
			newDataTypeArr: ['absolute_between'],   // absolute_between 绝对时间

			key: 'customAutoDateBetween',
		},

		// 相对当前时间点时
		relativeWithin: {
			dataTypeArr: ['datetime'],
			newDataTypeArr: ['relative_within'],   // relative_within 相对当前时间点

			key: ['customNumDate', 'customRelative'],
		},

		// 相对当前时间区间
		dayBetween: {
			dataTypeArr: ['datetime'],
			newDataTypeArr: ['relative_between'],   // relative_within 相对当前时间区间

			key: ['customNumDate_0', 'customNumDate_1'],
		},

		// 相对事件发生时间
		eventTime: {
			dataTypeArr: ['datetime'],
			newDataTypeArr: ['relative_event_time'],   // relative_event_time 相对时间发生时间

			key: ['customEventTime', 'customNumDate_2', 'customTimeUnit']
		},

	},

	option :{
		tooltip: {
			trigger: 'axis',
			axisPointer: {            // 坐标轴指示器，坐标轴触发有效
				type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
			}
		},
		grid: {
			top: '8%',   // 等价于 y: '16%'
			bottom: '16%',
			containLabel: true
		},
		legend: {
			type:"scroll",
			// orient 设置布局方式，默认水平布局，可选值：'horizontal'（水平） ¦ 'vertical'（垂直）
			orient: 'horizontal',
			// x 设置水平安放位置，默认全图居中，可选值：'center' ¦ 'left' ¦ 'right' ¦ {number}（x坐标，单位px）
			x: 'center',
			// y 设置垂直安放位置，默认全图顶端，可选值：'top' ¦ 'bottom' ¦ 'center' ¦ {number}（y坐标，单位px）
			y: 'bottom',
		},
		xAxis: [{
			data:{}
		}],
		yAxis: [
			{	minInterval:1,
				type: 'value'
			}
		],
		series: [
		]
	},


	tableData:{
		"byFields": [{
			"attrCode": "$city",
			"attrName": "城市",
			"attrType": "user",
			"dataType": "string"
		},
			{
				"attrCode": "$province",
				"attrName": "省份",
				"attrType": "user",
				"dataType": "string"
			}],
		"indicatorNames": ["$city"],
		"series": ["洪山区",
			"上海市",
			"武昌区",
			"广州市"],
		"rows": [{
			"byValues": ["卡布奇洛"],
			"values": [0,
				1,
				0,
				0]
		},
			{
				"byValues": ["湖南省"],
				"values": [0,
					0,
					1,
					0]
			},
			{
				"byValues": ["湖北省"],
				"values": [1,
					0,
					0,
					1]
			},
			{
				"byValues": ["乌鲁木齐"],
				"values": [0,
					0,
					0,
					1]
			},
			{
				"byValues": ["北京"],
				"values": [0,
					1,
					0,
					0]
			}]
	}
}

