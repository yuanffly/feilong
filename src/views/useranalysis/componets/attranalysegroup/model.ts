let uid = 0
/**
 * 用户属性
 */
export class UserProperty {
	id = uid++
	attr = {
		attrName: '城市',
		attrCode: '$city',
		attrType: 'user'
	}
	functionAlias = '等于'
	functionCode = 'equal'
	params = ['北京']
	conditions = []
	relation = 'and'
}

/**
 * 用户行为
 */
export class Behavior {
	id = uid++
	include = 'contain' //做过  notContain为未做过
	fromDate = '2020-12-12' //时间段的开始时间
	toDate = '2020-12-12' //时间段的结束时间
	eventAlias = '支付订单' //事件显示名 //事件列表接口获取
	eventCode = 'payProduct' //事件码 //事件列表接口获取
	eventAttr = {
		//所聚合的事件属性 当计算方式选择总次数、触发用户数、人均次数时 为空 //聚合属性列表获取
		attrCode: '',
		attrName: ''
	}
	aggregatorCode = 'unique' //聚合函数码 //从聚合函数列表接口获取
	aggregatorAlias = '触发用户数' //聚合函数别名
	filter = {
		//添加属性（筛选条件）
		conditions: [] /* 动态添加用户属性 */,
		relation: 'and' //同级conditions条件间的关系 and / or
	}
}

/**
 * 行为系列
 */

export class Series {
	id = uid++
	fromDate = '2021-08-01'
	toDate = '2021-08-12'
	steps = [/*动态添加单步骤事件，但至少会有两个步骤*/ new Step(), new Step()]
}

/**
 * 行为系列中单步骤事件
 */
export class Step {
	id = uid++
	eventCode = 'payProduct' //事件码
	eventAlias = '支付订单' //事件名称
	filter = {
		conditions: [] /* 动态添加用户属性 */,
		relation: 'and' //同级conditions条件间的关系 and / or
	}
}
