export const crowdRules = {
	allRelation: 'and',
	userAttrFilter: {
		//用户属性模块
		conditions: [],
		relation: 'and'
	},
	eventCrowds: [],
	eventRelation: 'and', //用户行为间的关系 and / or
	behaviorSeqRelation: 'and', //行为序列间的关系
	behaviorSeqVos: [] //行为序列
}

// let uid = 0
/**
 * 用户属性
 */
export class UserProperty {
	attr = {
		attrName: '',
		attrCode: '',
		attrType: ''
	}
	functionAlias = ''
	functionCode = ''
	params = []
	_timeRange = [null,null]
	_params1=''
	_params2=''
	_params3=''
	conditions = []
	relation = 'and'
}

/**
 * 用户行为
 */
export class Behavior {
	include = 'contain' //做过  notContain为未做过
	fromDate = '' //时间段的开始时间
	toDate = '' //时间段的结束时间
	_timeRange = [null,null]
	eventAlias = '' //事件显示名 //事件列表接口获取
	eventCode = '' //事件码 //事件列表接口获取
	eventAttr = {
		//所聚合的事件属性 当计算方式选择总次数、触发用户数、人均次数时 为空 //聚合属性列表获取
		attrCode: '',
		attrName: ''
	}
	aggregatorCode = '' //聚合函数码 //从聚合函数列表接口获取
	aggregatorAlias = '' //聚合函数别名
	_aggregator = []
	functionAlias = ''
	functionCode = ''
	params = []
	_params = ''
	_params1 = ''
	_params2 = ''
	filter = {
		//添加属性（筛选条件）
		conditions: [] /* 动态添加用户属性 */,
		relation: 'and' //同级conditions条件间的关系 and / or
	}
}

/**
 * 行为系列
 */

export class Series {
	fromDate = ''
	toDate = ''
	_timeRange = [null,null]
	steps = [new Step(), new Step()] /*动态添加单步骤事件，但至少会有两个步骤*/
}

/**
 * 行为系列中单步骤事件
 */
export class Step {
	eventCode = '' //事件码
	eventAlias = '' //事件名称
	filter = {
		conditions: [] /* 动态添加用户属性 */,
		relation: 'and' //同级conditions条件间的关系 and / or
	}
}

/**
 * 属性，事件值getAttrValues接口的请求参数
 */
export interface ValuesQueryInter {
	projectId: number | string
	attrType: string
	attrCode: string
	keywords: string
	size: number | string
}

/**
 * 去掉表单对象中的_开头的私有属性
 *
 */
export function deleteCustomAttrs(obj:any) {
	if (obj instanceof Object && obj !== null) {
		let keys = Object.keys(obj)
		for (let i = keys.length - 1; i > -1; i--) {
			let key = keys[i]
			if (key.indexOf('_') === 0) {
				delete obj[key]
			}
			deleteCustomAttrs(obj[key])
		}
		return obj
	}
}


interface Tokey {
	[key: string]: any
}

/**
 * 反显数据
 */
export function backShowData(form: Tokey, data: Tokey) {
	Object.keys(form).forEach(key => {
		if (Object.prototype.toString.call(form[key]) !== "[object Object]" ) {
			form[key] = data[key]
		} else {
			backShowData(form[key], data[key])
		}
	})
	return form
}
