const now: Date = new Date() // 当前日期
const nowDayOfWeek: number = now.getDay() // 今天本周的第几天
const nowDay: number = now.getDate() // 当前日
const nowMonth: number = now.getMonth() // 当前月

const lastMonthDate: Date = new Date() // 上月日期
lastMonthDate.setDate(1)
lastMonthDate.setMonth(lastMonthDate.getMonth() - 1)
const lastMonth = lastMonthDate.getMonth()

let nowYear: number = now.getFullYear() // 当前年
nowYear += nowYear < 2000 ? 1900 : 0

// 格式化日期：yyyy-MM-dd
function formatDate(date: Date): string {
	const myyear = date.getFullYear()
	let mymonth: number | string = date.getMonth() + 1
	let myweekday: number | string = date.getDate()

	if (mymonth < 10) {
		mymonth = '0' + mymonth
	}
	if (myweekday < 10) {
		myweekday = '0' + myweekday
	}
	return myyear + '-' + mymonth + '-' + myweekday
}

// 获得某月的天数
function getMonthDays(myMonth: number): number {
	const monthStartDate: any = new Date(nowYear, myMonth, 1)
	const monthEndDate: any = new Date(nowYear, myMonth + 1, 1)
	const days: number = (monthEndDate - monthStartDate) / (1000 * 60 * 60 * 24)
	return days
}

//获得本季度的开始月份
export function getQuarterStartMonth(): number {
	let quarterStartMonth = 0
	if (nowMonth < 3) {
		quarterStartMonth = 0
	}
	if (2 < nowMonth && nowMonth < 6) {
		quarterStartMonth = 3
	}
	if (5 < nowMonth && nowMonth < 9) {
		quarterStartMonth = 6
	}
	if (nowMonth > 8) {
		quarterStartMonth = 9
	}
	return quarterStartMonth
}

// 获得本周的开始日期
export function getWeekStartDate(): string {
	const weekStartDate = new Date(nowYear, nowMonth, nowDay - nowDayOfWeek + 1)
	return formatDate(weekStartDate)
}

// 获得本周的结束日期
export function getWeekEndDate(): string {
	const weekEndDate = new Date(nowYear, nowMonth, nowDay + (7 - nowDayOfWeek))
	return formatDate(weekEndDate)
}

// 获得上周的开始日期
export function getLastWeekStartDate(): string {
	const weekStartDate = new Date(nowYear, nowMonth, nowDay - nowDayOfWeek - 6)
	return formatDate(weekStartDate)
}

// 获得上周的结束日期
export function getLastWeekEndDate(): string {
	const weekEndDate = new Date(nowYear, nowMonth, nowDay - nowDayOfWeek)
	return formatDate(weekEndDate)
}

//获得本月的开始日期
export function getMonthStartDate(): string {
	const monthStartDate = new Date(nowYear, nowMonth, 1)
	return formatDate(monthStartDate)
}

// 获得本月的结束日期
export function getMonthEndDate(): string {
	const monthEndDate = new Date(nowYear, nowMonth, getMonthDays(nowMonth))
	return formatDate(monthEndDate)
}

// 获得上月开始时间
export function getLastMonthStartDate(): string {
	const lastMonthStartDate = new Date(nowYear, lastMonth, 1)
	return formatDate(lastMonthStartDate)
}

// 获得上月结束时间
export function getLastMonthEndDate(): string {
	const lastMonthEndDate = new Date(nowYear, lastMonth, getMonthDays(lastMonth))
	return formatDate(lastMonthEndDate)
}

// 获得本季度的开始日期
export function getQuarterStartDate(): string {
	const quarterStartDate = new Date(nowYear, getQuarterStartMonth(), 1)
	return formatDate(quarterStartDate)
}

// 获得本季度的结束日期
export function getQuarterEndDate(): string {
	const quarterEndMonth = getQuarterStartMonth() + 2
	const quarterStartDate = new Date(nowYear, quarterEndMonth, getMonthDays(quarterEndMonth))
	return formatDate(quarterStartDate)
}

// 获取距离当前日期过去n天的日期（如过去7天， 传入-6）
export function getDate(n: number): string {
	const now: Date = new Date()
	const calDate: Date = new Date(now)
	// let num: number = n;
	// if (num > 0) {
	//   num = num - 1;
	// } else {
	//   num = num + 1;
	// }
	calDate.setDate(now.getDate() + n)
	let month: string | number = calDate.getMonth() + 1
	let date: string | number = calDate.getDate()
	if (month < 10) {
		month = `0${month}`
	}
	if (date < 10) {
		date = `0${date}`
	}
	const time = calDate.getFullYear() + '-' + month + '-' + date
	return time
}
//时间转化
export function time(val: any) {
	if (val) {
		let date = new Date(val)
		let y = date.getFullYear()
		let M = (date.getMonth() + 1) >= 10 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)
		let d = date.getDate() >= 10 ? date.getDate() : '0' + date.getDate()
		let h = date.getHours() >= 10 ? date.getHours() : '0' + date.getHours()
		let m = date.getMinutes() >= 10 ? date.getMinutes() : '0' + date.getMinutes()
		let s = date.getSeconds() >= 10 ? date.getSeconds() : '0' + date.getSeconds()
		return `${y}-${M}-${d}  ${h}:${m}:${s}`
	} else {
		return ''
	}

}
