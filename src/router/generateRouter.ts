/*
  path:
  name:'router-name'            当使用<keep-alive>时，name字段是必需的，它也应该与组件的name属性匹配
  meta: {
	roles: ['admin', 'editor']   将控制页面角色(允许设置多个角色)
	title: 'title'               在subMenu和breadcrumb中显示的名称(推荐集)
	icon: 'svg-name'             侧边栏中显示的图标
	root: true         	         如果为true，将始终显示根菜单(默认为false)
								               如果为false，隐藏根菜单时，有小于或等于一个子路由
	breadcrumb: false            如果为false，该项将隐藏在面包屑中(默认为true)
	sidebar: true                如果为true，显示sidebar
  }
*/


const SelfQuery = () => import('@/views/behavioural/selfquery/SelfQuery.vue') //sql查询
const SelfList = () => import('@/views/behavioural/selflist/SelfList.vue') //查询列表

const UserGroup = () => import('@/views/useranalysis/UserGroup.vue')//用户分群
const AttrAnalyse = () => import('@/views/useranalysis/AttrAnalyse.vue') //属性分析

const Help = () => import('@/views/profile/Help.vue') //使用指南
const Profile = () => import('@/views/profile/Profile.vue') //个人中心

export const routerDatas = [
	{
		path: '/',
		redirect: '/useranalyse',
		meta:{
			root:false
		}
	},
	{
		path: '/useranalysis',
		redirect: '/useranalysis/usergroup',
		component: { render: (e: any) => e('router-view') },
		meta: {
			title: '用户分析',
			name: 'UserAnalyse',
			icon: '',
			root: true,
			sidebar: true
		},
		children: [
			{
				path: '/useranalysis/usergroup',
				name: 'UserGroup',
				component: UserGroup,
				meta: {
					title: '用户分群',
					icon: 'icon-route',
					sidebar: true
				}
			},
			{
				path: '/useranalysis/attrs',
				name: 'AttrAnalyse',
				component: AttrAnalyse,
				meta: {
					title: '属性分析',
					icon: 'icon-funnel',
					sidebar: true
				}
			}
		]
	},
	{
		path: '/behavioural',
		redirect: '/behavioural/selfquery',
		component: { render: (e: any) => e('router-view') },
		meta: {
			title: '自定义分析',
			name: 'Behavioural',
			icon: '',
			root: true,
			sidebar: true
		},
		children: [
			{
				path: '/behavioural/selfquery',
				name: 'SelfQuery',
				component: SelfQuery,
				meta: {
					title: '自定义',
					icon: 'icon-custom',
					sidebar: true
				}
			},
			{
				path: '/behavioural/selflist',
				name: 'SelfList',
				component: SelfList,
				meta: {
					title: '查询列表',
					icon: 'icon-data',
					sidebar: true
				}
			}
		]
	},

	{
		path: '/accountinfo',
		redirect: '/accountinfo/profile',
		component: { render: (e: any) => e('router-view') },
		meta: {
			title: '账号信息',
			name: 'Profile',
			icon: '',
			root: true,
			sidebar: true
		},
		children: [
			{
				path: '/accountinfo/profile',
				name: 'Profile',
				component: Profile,
				meta: {
					title: '个人中心',
					icon: 'icon-point',
					sidebar: true
				}
			},
			{
				path: '/accountinfo/help',
				name: 'Help',
				component: Help,
				meta: {
					title: '帮组文档',
					icon: 'icon-help',
					sidebar: true
				}
			}
		]
	}
]

const routers = routerDatas
export default routers
