import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './generateRouter'

Vue.use(VueRouter)

const createRouter = () =>
	new VueRouter({
		mode: 'hash',
		scrollBehavior: (to, from, savedPosition) => {
			if (savedPosition) {
				return savedPosition
			} else {
				return { x: 0, y: 0 }
			}
		},
		base: process.env.BASE_URL,
		routes
	})

const router: any = createRouter()

export function resetRouter(params: []) {
	const newRouter = createRouter()
	;(router as any).matcher = (newRouter as any).matcher
	params.forEach(val => {
		router.addRoute(val)
	})
}
router.resetRouter = resetRouter
export default router
