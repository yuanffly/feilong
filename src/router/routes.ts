/** @format */

import { RouteConfig } from 'vue-router'

// const Home = () => import('@/views/Home.vue')
const Events = () => import('@/views/behavioural/events/Events.vue')
const Retained = () => import('@/views/behavioural/retained/Retained.vue')
const AdminTable = () => import('@/views/datamanagement/eventAdmin.vue')
const EventArrtAdmin = () => import('@/views/datamanagement/eventArrtAdmin.vue')
const userArrtAdmin = () => import('@/views/datamanagement/userArrtAdmin.vue')
const SequenceList = () => import('@/views/sequence/list/index.vue')
const SequenceDetail = () => import('@/views/sequence/detail/index.vue')
const Project = () => import('@/views/project/index.vue')

const routes: Array<RouteConfig> = [
  {
    path: '/',
    redirect: '/behavioural/events'
  },
  {
    path: '/databoard',
    meta: {
      title: '数据看板',
      name: 'Databoard',
      icon: '',
      root: true,
      sidebar: true
    },
    children: []
  },
  {
    path: '/behavioural',
    name: 'Behavioural',
    redirect: '/behavioural/events',
    component: { render: (e: any) => e('router-view') },
    children: [
      {
        path: '/behavioural/events',
        name: 'Events',
        component: Events,
        meta: {
          title: '事件',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/behavioural/funnel',
        name: 'Funnel',
        meta: {
          title: '漏斗',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/behavioural/retained',
        name: 'Retained',
        component: Retained,
        meta: {
          title: '留存',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/behavioural/distribution',
        meta: {
          title: '分布',
          name: 'Distribution',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/behavioural/paths',
        meta: {
          title: '路径',
          name: 'Paths',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/behavioural/selfquery',
        meta: {
          title: '自定义',
          name: 'SelfQuery',
          icon: '',
          sidebar: true
        }
      }
    ]
  },
  {
    path: '/useranalysis',
    redirect: '/useranalysis/attrsanalysis',
    component: { render: (e: any) => e('router-view') },
    meta: {
      title: '用户分析',
      // name: 'UserAnalysis',
      icon: '',
      root: true
    },
    children: [
      {
        path: '/useranalysis/attrs',
        meta: {
          title: '属性',
          // name: 'Attrs',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/useranalysis/usergroup',
        meta: {
          title: '分群',
          // name: 'UserGroup',
          icon: '',
          sidebar: true
        }
      }
    ]
  },
  {
    path: '/datamanagement',
    redirect: '/datamanagement/segmentation',
    component: { render: (e: any) => e('router-view') },
    meta: {
      title: '数据管理',
      name: 'DataManagement',
      icon: '',
      root: true
    },
    children: [
      {
        path: '/datamanagement/segmentation',
        component: AdminTable,
        meta: {
          title: '事件',
          name: 'Segmentation',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/datamanagement/segmentationattrs',
        component: EventArrtAdmin,
        meta: {
          title: '事件属性',
          name: 'SegmentationAttrs',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/datamanagement/userattrs',
        component: userArrtAdmin,
        meta: {
          title: '用户属性',
          name: 'UserAttrs',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/datamanagement/virtual',
        meta: {
          title: '虚拟事件',
          name: 'Virtual',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/datamanagement/warning',
        meta: {
          title: '预警管理',
          name: 'Warning',
          icon: '',
          sidebar: true
        }
      },
      {
        path: '/datamanagement/sign',
        meta: {
          title: '埋点管理',
          name: 'Sign',
          icon: '',
          sidebar: true
        }
      }
    ]
  },
  {
    path: '/adminTable',
    name: 'adminTable',
    component: AdminTable
  },
  {
    path: '/sequence',
    name: 'sequence',
    redirect: '/sequence/sequenceList',
    // component: Layout,
    component: { render: e => e('router-view') },

    children: [
      {
        path: 'sequenceList',
        name: 'sequenceList',
        meta: {
          title: '用户列表',
          icon: '',
          sidebar: false
        },
        component: SequenceList
      },
      {
        path: 'sequenceDetail',
        name: 'sequenceDetail',
        meta: {
          title: '用户序列',
          icon: '',
          sidebar: false
        },
        component: SequenceDetail
      }
    ]
  },
  {
    path: '/project',
    name: 'project',
    component: Project,
    meta: {
      title: '项目管理',
      sidebar: true
    }
  }
]

export default routes
