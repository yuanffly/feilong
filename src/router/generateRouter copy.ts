/*
  path:
  name:'router-name'            当使用<keep-alive>时，name字段是必需的，它也应该与组件的name属性匹配
  meta: {
	roles: ['admin', 'editor']   将控制页面角色(允许设置多个角色)
	title: 'title'               在subMenu和breadcrumb中显示的名称(推荐集)
	icon: 'svg-name'             侧边栏中显示的图标
	root: true         	         如果为true，将始终显示根菜单(默认为false)
								               如果为false，隐藏根菜单时，有小于或等于一个子路由
	breadcrumb: false            如果为false，该项将隐藏在面包屑中(默认为true)
	sidebar: true                如果为true，显示sidebar
  }
*/

const Events = () => import('@/views/behavioural/events/Events.vue')
const SelfQuery = () => import('@/views/behavioural/selfquery/SelfQuery.vue')

const UserAnalyse = () => import('@/views/useranalysis/UserAnalyse.vue')
const AttrAnalyse = () => import('@/views/useranalysis/AttrAnalyse.vue')

const Help = () => import('@/views/help/Help.vue')
const Profile = () => import('@/views/profile/Profile.vue')

export const routerDatas = [
	{
		path: '/behavioural',
		redirect: '/behavioural/events',
		component: { render: (e: any) => e('router-view') },
		meta: {
			title: '行为分析',
			name: 'Behavioural',
			icon: '',
			root: true,
			sidebar: true
		},
		children: [
			{
				path: '/behavioural/events',
				name: 'Events',
				component: Events,
				meta: {
					title: '事件',
					icon: 'icon-event',
					sidebar: true
				}
			},
			{
				path: '/behavioural/selfquery',
				name: 'SelfQuery',
				component: SelfQuery,
				meta: {
					title: '自定义',
					icon: 'icon-custom',
					sidebar: true
				}
			}
		]
	},
	{
		path: '/useranalyse',
		redirect: '/useranalyse',
		component: { render: (e: any) => e('router-view') },
		meta: {
			title: '用户分群',
			name: 'UserAnalyse',
			icon: '',
			root: true,
			sidebar: true
		},
		children: [
			{
				path: '/userAnalyse',
				name: 'UserAnalyse',
				component: UserAnalyse,
				meta: {
					title: '用户分群',
					icon: 'icon-event',
					sidebar: true
				}
			},
			{
				path: '/useranalyse/attranalyse',
				name: 'AttrAnalyse',
				component: AttrAnalyse,
				meta: {
					title: '属性分析',
					icon: 'icon-event',
					sidebar: true
				}
			}
		]
	},
	{
		path: '/help',
		redirect: '/help',
		component: { render: (e: any) => e('router-view') },
		meta: {
			title: '帮组文档',
			name: 'Help',
			icon: '',
			root: false,
			sidebar: false
		},
		children: [
			{
				path: '/help',
				name: 'Help',
				component: Help,
				meta: {
					title: '帮组文档',
					icon: 'icon-event',
					sidebar: false
				}
			}
		]
	},
	{
		path: '/profile',
		redirect: '/profile',
		component: { render: (e: any) => e('router-view') },
		meta: {
			title: '个人中心',
			name: 'Profile',
			icon: '',
			root: false,
			sidebar: false
		},
		children: [
			{
				path: '/profile',
				name: 'Profile',
				component: Profile,
				meta: {
					title: '个人中心',
					icon: 'icon-event',
					sidebar: false
				}
			}
		]
	}
]

const routers = routerDatas
export default routers
