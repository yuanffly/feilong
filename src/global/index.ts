import Vue from 'vue'
import request from '@/utils/request'
import moment from 'moment'
import 'moment/locale/zh-cn'
import { notification } from 'ant-design-vue'
moment.locale('zh-cn')
export default {
  install($Vue: { [key: string]: any }) {
		const bus = new Vue()
		$Vue.prototype.$bus = bus
		$Vue.prototype.$moment = moment
		$Vue.prototype.$request = request
		$Vue.prototype.$notify = notification
  }
}
