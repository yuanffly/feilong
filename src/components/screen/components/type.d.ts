// 指数
interface Events {
	eventCode: string
	eventAlias: string
}

export interface EventsListType {
	groupName: string
	events: Events[]
}

// 指数 属性
interface AnalysisType {
	aggregatorCode: string
	aggregatorAlias: string
}

interface AttrPropType {
	attrName: string
	attrCode: string
	dataType: string
	isNull: number
	hasDict: number
	analysis?: AnalysisType[]
}

export interface EventsAttrListType {
	props: AttrPropType[] | []
	staidQuots: AnalysisType[] | []
}

interface AttrType {
	dataType: string
	attrType: string
	attrCode: string // 条件的字段 XX / XX
	attrName: string
}
interface ConditionType {
	renderIndex: number
	relation: string
	attr: {
		dataType: string
		attrType: string
		attrCode: string // 条件的字段 XX / XX
		attrName: string
	}
	functionCode: string // 关系
	functionAlias: string // 关系
	params: string[]
	conditions: ConditionType[]
}
// 	filters: FilterType[]
interface FilterType {
	relation?: string
	conditions: ConditionType[]
}

interface EventAttrType {
	attrCode: string
	attrName: string
	attrType: string
}
interface IndicatorsType {
	indicatorName: string
	eventAlias: string
	eventCode: string
	eventAttr: EventAttrType
	aggregatorCode?: string
	aggregatorAlias?: string
	filter: FilterType[]
}
export interface FieldsType {
	size: number
	current: number
	projectId: number
	unit: string
	toDate: string
	fromDate: string
	indicators: IndicatorsType[]
	filter: FilterType
	byFields: AttrType[] | []
}
