// 指数默认
export const defaultIindicators: { [key: string]: any } = {
	indicatorName: '任意事件的总次数',
	eventAlias: '任意事件',
	eventCode: 'anything',
	eventAttr: {
		attrCode: '',
		attrName: ''
	},
	aggregatorCode: 'general',
	aggregatorAlias: '总次数'
}
// 条件默认
export const defaultCondition: { [key: string]: any } = {
	attr: {
		attrCode: 'event_name',
		attrName: '事件名称',
		attrType: 'event',
		dataType: 'string'
	},
	functionCode: 'equal',
	functionAlias: '等于',
	params: []
}
// 分组默认
export const defaultGroups = {
	attrCode: 'event_name',
	attrName: '事件名称',
	attrType: 'event',
	dataType: 'string'
}

// 事件分析默认
export const defaultData = {
	isShowTit: null,
	// 指数、条件
	indicators: [
		{
			...defaultIindicators,
			filter: {
				conditions: []
			}
		}
	],
	// 全局筛选
	filter: {
		conditions: []
	},
	// 全体
	byFields: [
		// {
		// 	dataType: 'string',
		// 	attrType: 'event',
		// 	attrCode: 'defaultTotality',
		// 	attrName: '总体'
		// },
		{
			attrName: '用户id',
			attrCode: 'user_id',
			dataType: 'number',
			intervals: [2, 4, 6]
		}
	]
}

// 事件分析默认
export const defaultVal = {
	indicators: [
		{
			indicatorName: '任意事件的总次数',
			eventAlias: '任意事件',
			eventCode: 'anything',
			aggregatorCode: 'event_name',
			aggregatorAlias: '事件名称',
			eventAttr: {
				attrCode: '',
				attrName: ''
			},
			filter: {
				conditions: []
			}
		}
	],
	// 全局
	filter: {
		conditions: []
	},
	// 总体
	byFields: [
		{
			dataType: 'string',
			attrType: 'event',
			attrCode: 'all',
			attrName: '总体'
		}
	]
}

// 漏斗
export const defaultFunnelVal = {
	projectId: 60,
	fromDate: 1627303359000,
	toDate: 1627648959000,
	// 筛选条件
	steps: [
		{
			customName: '浏览',
			eventAlias: '浏览',
			eventCode: 'browse',
			filter: {
				relation: 'and',
				conditions: [
					{
						attr: {
							attrName: '城市',
							attrCode: '$city',
							attrType: 'event'
						},
						functionAlias: '等于',
						functionCode: 'equal',
						params: ['上海', '北京', '武汉']
					}
				]
			}
		},
		{
			customName: '点击',
			eventAlias: '点击',
			eventCode: 'click',
			filter: {
				conditions: []
			}
		},
		{
			customName: '购买',
			eventAlias: '购买',
			eventCode: 'pay',
			filter: {
				conditions: []
			}
		}
	],
	// 全局
	filter: {
		conditions: []
	},
	// 分组项
	byFields: [
		{
			attrCode: '$os',
			attrName: '操作系统',
			attrType: 'event'
		}
	],
	// 窗口期
	maxConvertTime: 3600 //单位 秒
}

// 留存
export const defaultRetainedData = {
	projectId: 60,
	fromDate: 1627303359000,
	toDate: 1627648959000,
	// 新增时间单位
	timeUnit: 'month', //'month' / 'week' / day
	// 新增周期数
	period: 7,
	// 新增查询类型
	queryType: 'retention', //'retention' / 'wastage'
	// 初始行为
	initBehaviour: {
		eventCode: 'browse',
		eventAlias: '浏览',
		filter: {
			relation: 'and',
			conditions: [
				{
					attr: {
						attrName: '城市',
						attrCode: '$city',
						attrType: 'event'
					},
					functionAlias: '等于',
					functionCode: 'equal',
					params: ['上海', '北京', '武汉']
				}
			]
		}
	},
	// 后续行为
	followBehaviour: {
		eventCode: 'browse',
		eventAlias: '浏览',
		filter: {
			relation: 'and',
			conditions: [
				{
					attr: {
						attrName: '城市',
						attrCode: '$city',
						attrType: 'event'
					},
					functionAlias: '等于',
					functionCode: 'equal',
					params: ['上海', '北京', '武汉']
				}
			]
		}
	},
	// 全局
	filter: {
		conditions: []
	},
	// 分组项
	byFields: [
		{
			attrCode: '$os',
			attrName: '操作系统',
			attrType: 'event'
		}
	]
}
