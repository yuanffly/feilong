export { default as Notice } from './notice/Notice.vue'
export { default as Task } from './task/Task.vue'
export { default as Tips } from './tips/Tips.vue'

export { default as Echarts } from './echarts/index.vue'
