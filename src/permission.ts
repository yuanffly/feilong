import router from './router'
import { UserRoleModule } from '@/store/modules/userRole'

router.beforeEach(async (to: { [key: string]: any }, from: { [key: string]: any }, next: any) => {
	const roleList: any[] = UserRoleModule.role
	if (roleList.length) {
		// 当前用户已拉取权限信息
		next()
	} else {
		// 当前用户未拉取权限信息
		await UserRoleModule.getRole()
		await UserRoleModule.generateRoutes()
		const routers: any = UserRoleModule.routers
		// routers.push({
		// 	path: '*',
		// 	redirect: '/notFound'
		// })
		router.resetRouter(routers)
		router.options.routes = routers
		if (to.path === '/') {
			next({ path: '/behavioural/events', replace: true }) // hack方法 确保addRoutes已完成
		} else {
			next({ ...to, replace: true }) // hack方法 确保addRoutes已完成
		}
	}
	// next()
})
