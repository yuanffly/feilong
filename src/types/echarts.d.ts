export interface Option {
  [k:string]: any
}

export interface Config {
  [k:string]: any
}

export interface ChartComps {
  [k:string]: any
}
