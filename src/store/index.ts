import Vue from 'vue'
import Vuex from 'vuex'
// import getters from './getters'

Vue.use(Vuex)

// const modulesFiles = require.context('./modules', true, /\.ts$/)

// const modules = modulesFiles.keys().reduce((modules: { [key: string]: any }, modulePath: string) => {
//   const moduleName: string = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
//   const value = modulesFiles(modulePath)
//   modules[moduleName] = value.default
//   return modules
// }, {})
const store= new Vuex.Store({
})

export default store

