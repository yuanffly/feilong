/* eslint-disable */
/* tslint:disable */
// @ts-ignore
import icon from 'vue-svgicon'
icon.register({
	'arrow-b': {
		width: 114,
		height: 56,
		viewBox: '0 0 114 56',
		data: '<g id="设计" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="2-1-漏斗分析" transform="translate(-905.000000, -303.000000)" fill="#2085EF"><g id="编组-35" transform="translate(905.000000, 303.000000)"><path d="M22,0 L92,0 C93.1045695,-2.02906125e-16 94,0.8954305 94,2 L94,22 L94,22 L114,22 L59.0491132,54.7777219 C57.7867935,55.5306846 56.2132065,55.5306846 54.9508868,54.7777219 L0,22 L0,22 L20,22 L20,2 C20,0.8954305 20.8954305,2.02906125e-16 22,0 Z" id="矩形"></path></g></g></g>'
	}
})
