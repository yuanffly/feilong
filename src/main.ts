import Vue from 'vue'
import App from './App.vue'
import Antd from 'ant-design-vue'
import SvgIcon from 'vue-svgicon'

import '@/icons/components'
import 'ant-design-vue/dist/antd.css'
import './assets/styles/index.scss'
import '@/assets/iconfont/iconfont.css'
// import '@/permission'  //屏蔽通过权限获取路由
import router from './router'
import store from './store'
import global from './global'

declare module 'vue/types/vue' {
	interface Vue {
		$moment: any
		$request: any
		$bus: any
		$notify: any
	}
}

Vue.use(global)

Vue.use(Antd)

Vue.use(SvgIcon, {
	tagName: 'svg-icon',
	defaultWidth: '1em',
	defaultHeight: '1em'
})

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => {
		return h(App)
	}
}).$mount('#app')
