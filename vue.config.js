/** @format */

let proxyUrl = ''
if (process.env.NODE_ENV === 'production') {
	proxyUrl = 'http://172.23.7.57:9099/'
} else if (process.env.NODE_ENV === 'development') {
	// proxyUrl = 'http://www.x3-clover.com/'
	proxyUrl ='http://172.23.7.56:50081/'
}

module.exports = {
	publicPath: process.env.VUE_APP_PUBLIC_PATH,
	css: {
		loaderOptions: {
			less: {
				lessOptions: {
					javascriptEnabled: true
				}
			},
			sass: {
				sassOptions: {
					data: `@import "@/assets/styles/variables.scss";`
				},
				prependData: `@import "@/assets/styles/variables.scss";`
			}
		}
	},
	// 路径别名配置
	configureWebpack: {
		resolve: {
			alias: {
				assets: '@/assets',
				common: '@/common',
				components: '@/components',
				network: '@/network',
				views: '@/views'
			}
		}
	},
	devServer: {
		port: 8080,
		proxy: {
			'/reportservice': {
				target: proxyUrl,
				secure: false,
				changeOrigin: true
			}
		}
	}
}
