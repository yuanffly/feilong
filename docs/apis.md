# 飞龙项目reportService接口文档


**简介**:飞龙项目reportService接口文档


**HOST**:172.23.7.56:9099


**联系人**:


**Version**:1.0


**接口路径**:undefined


[TOC]






# 留存分析


## 留存分析-报表查询


**接口地址**:`/reportservice/retention/report`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "byFields": [
    {
      "attrCode": "event_time",
      "attrName": "事件触发时间",
      "attrType": "1",
      "dataType": "number",
      "intervals": "[0,100,300]"
    }
  ],
  "filter": {
    "conditions": [
      {
        "attr": {
          "attrCode": "event_time",
          "attrName": "事件触发时间",
          "attrType": "1",
          "dataType": "number",
          "intervals": "[0,100,300]"
        },
        "conditions": "[]",
        "functionAlias": "date",
        "functionCode": "EQUAL(\"等于\", \"equal\")",
        "params": "[]",
        "relation": "and",
        "renderIndex": 2
      }
    ],
    "relation": ""
  },
  "followBehaviour": {
    "eventCode": "",
    "filter": {
      "conditions": [
        {
          "attr": {
            "attrCode": "event_time",
            "attrName": "事件触发时间",
            "attrType": "1",
            "dataType": "number",
            "intervals": "[0,100,300]"
          },
          "conditions": "[]",
          "functionAlias": "date",
          "functionCode": "EQUAL(\"等于\", \"equal\")",
          "params": "[]",
          "relation": "and",
          "renderIndex": 2
        }
      ],
      "relation": ""
    }
  },
  "fromDate": "",
  "initBehaviour": {
    "eventCode": "",
    "filter": {
      "conditions": [
        {
          "attr": {
            "attrCode": "event_time",
            "attrName": "事件触发时间",
            "attrType": "1",
            "dataType": "number",
            "intervals": "[0,100,300]"
          },
          "conditions": "[]",
          "functionAlias": "date",
          "functionCode": "EQUAL(\"等于\", \"equal\")",
          "params": "[]",
          "relation": "and",
          "renderIndex": 2
        }
      ],
      "relation": ""
    }
  },
  "period": 0,
  "projectId": 0,
  "queryType": "",
  "timeUnit": "",
  "toDate": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|retentionRequest|留存分析|body|true|RetentionRequest|RetentionRequest|
|&emsp;&emsp;byFields|分组项||false|array|Attr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;filter|全局筛选||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;followBehaviour|后续行为||true|Behaviour|Behaviour|
|&emsp;&emsp;&emsp;&emsp;eventCode|行为事件||true|string||
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;fromDate|开始时间||true|string(date-time)||
|&emsp;&emsp;initBehaviour|初始行为||true|Behaviour|Behaviour|
|&emsp;&emsp;&emsp;&emsp;eventCode|行为事件||true|string||
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;period|周期数||true|integer(int32)||
|&emsp;&emsp;projectId|应用id||true|integer(int32)||
|&emsp;&emsp;queryType|查询类型,可用值:RETENTION,WASTAGE||true|string||
|&emsp;&emsp;timeUnit|时间单位,可用值:DAY,WEEK,MONTH||true|string||
|&emsp;&emsp;toDate|终止时间||true|string(date-time)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«RetentionVo»»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|RetentionVo|
|&emsp;&emsp;basicData|基础数据 MAP[分组字段, value]|object||
|&emsp;&emsp;date|日期|string||
|&emsp;&emsp;groupData|分组详情数据|array|RetentionVo|
|&emsp;&emsp;records|周期留存数据 List[List(total, percentage)]|array|array|
|&emsp;&emsp;total|总人数|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"basicData": {},
			"date": "",
			"groupData": [
				{
					"basicData": {},
					"date": "",
					"groupData": [
						{}
					],
					"records": [],
					"total": ""
				}
			],
			"records": [],
			"total": ""
		}
	],
	"msg": ""
}
```


# 账户管理


## 获取用户信息


**接口地址**:`/reportservice/account`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«User»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|User|User|
|&emsp;&emsp;createTime||string(date-time)||
|&emsp;&emsp;createUserId||integer(int32)||
|&emsp;&emsp;departmentId||string||
|&emsp;&emsp;departmentName||string||
|&emsp;&emsp;desc||string||
|&emsp;&emsp;email||string||
|&emsp;&emsp;gender||object||
|&emsp;&emsp;id||integer(int32)||
|&emsp;&emsp;loginName||string||
|&emsp;&emsp;mobile||string||
|&emsp;&emsp;name||string||
|&emsp;&emsp;partnerFullName||string||
|&emsp;&emsp;password||string||
|&emsp;&emsp;phone||string||
|&emsp;&emsp;proxyUmid||string||
|&emsp;&emsp;qq||string||
|&emsp;&emsp;rid||integer(int32)||
|&emsp;&emsp;status||integer(int32)||
|&emsp;&emsp;telphone||string||
|&emsp;&emsp;tenantAdmin||boolean||
|&emsp;&emsp;tenantId||integer(int32)||
|&emsp;&emsp;title||string||
|&emsp;&emsp;umid||string||
|&emsp;&emsp;updateTime||string(date-time)||
|&emsp;&emsp;updateUserId||integer(int32)||
|&emsp;&emsp;userDesc||string||
|&emsp;&emsp;userName||string||
|&emsp;&emsp;userPassword||string||
|&emsp;&emsp;wechat||string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"createTime": "",
		"createUserId": 0,
		"departmentId": "",
		"departmentName": "",
		"desc": "",
		"email": "",
		"gender": {},
		"id": 0,
		"loginName": "",
		"mobile": "",
		"name": "",
		"partnerFullName": "",
		"password": "",
		"phone": "",
		"proxyUmid": "",
		"qq": "",
		"rid": 0,
		"status": 0,
		"telphone": "",
		"tenantAdmin": true,
		"tenantId": 0,
		"title": "",
		"umid": "",
		"updateTime": "",
		"updateUserId": 0,
		"userDesc": "",
		"userName": "",
		"userPassword": "",
		"wechat": ""
	},
	"msg": ""
}
```


## 修改账户密码


**接口地址**:`/reportservice/account/rest/password`


**请求方式**:`PUT`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "newPassword": "",
  "oldPassword": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|param|param|body|true|PasswordParamVo|PasswordParamVo|
|&emsp;&emsp;newPassword|新密码||false|string||
|&emsp;&emsp;oldPassword|旧密码||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«string»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


## 更新用户信息


**接口地址**:`/reportservice/account/rest/user`


**请求方式**:`PATCH`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createTime": "",
  "createUserId": 0,
  "departmentId": "",
  "departmentName": "",
  "desc": "",
  "email": "",
  "gender": {},
  "id": 0,
  "loginName": "",
  "mobile": "",
  "name": "",
  "partnerFullName": "",
  "password": "",
  "phone": "",
  "proxyUmid": "",
  "qq": "",
  "rid": 0,
  "status": 0,
  "telphone": "",
  "tenantAdmin": true,
  "tenantId": 0,
  "title": "",
  "umid": "",
  "updateTime": "",
  "updateUserId": 0,
  "userDesc": "",
  "userName": "",
  "userPassword": "",
  "wechat": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|user|user|body|true|User|User|
|&emsp;&emsp;createTime|||false|string(date-time)||
|&emsp;&emsp;createUserId|||false|integer(int32)||
|&emsp;&emsp;departmentId|||false|string||
|&emsp;&emsp;departmentName|||false|string||
|&emsp;&emsp;desc|||false|string||
|&emsp;&emsp;email|||false|string||
|&emsp;&emsp;gender|||false|object||
|&emsp;&emsp;id|||false|integer(int32)||
|&emsp;&emsp;loginName|||false|string||
|&emsp;&emsp;mobile|||false|string||
|&emsp;&emsp;name|||false|string||
|&emsp;&emsp;partnerFullName|||false|string||
|&emsp;&emsp;password|||false|string||
|&emsp;&emsp;phone|||false|string||
|&emsp;&emsp;proxyUmid|||false|string||
|&emsp;&emsp;qq|||false|string||
|&emsp;&emsp;rid|||false|integer(int32)||
|&emsp;&emsp;status|||false|integer(int32)||
|&emsp;&emsp;telphone|||false|string||
|&emsp;&emsp;tenantAdmin|||false|boolean||
|&emsp;&emsp;tenantId|||false|integer(int32)||
|&emsp;&emsp;title|||false|string||
|&emsp;&emsp;umid|||false|string||
|&emsp;&emsp;updateTime|||false|string(date-time)||
|&emsp;&emsp;updateUserId|||false|integer(int32)||
|&emsp;&emsp;userDesc|||false|string||
|&emsp;&emsp;userName|||false|string||
|&emsp;&emsp;userPassword|||false|string||
|&emsp;&emsp;wechat|||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|204|No Content||
|401|Unauthorized||
|403|Forbidden||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


# 看板文件夹


## selectChart


**接口地址**:`/reportservice/folder/{projectId}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>查询所有的看板文件夹</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|path|true|integer(int32)||
|chartGroupName|看板名|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«FolderDto»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|FolderDto|
|&emsp;&emsp;chartGroups|看板列表|array|ChartGroupDto|
|&emsp;&emsp;&emsp;&emsp;chartGroupId|看板ID|integer||
|&emsp;&emsp;&emsp;&emsp;chartGroupName|看板名|string||
|&emsp;&emsp;folderId|看板组ID|integer(int32)||
|&emsp;&emsp;folderName|看板组名|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"chartGroups": [
				{
					"chartGroupId": 0,
					"chartGroupName": ""
				}
			],
			"folderId": 0,
			"folderName": ""
		}
	],
	"msg": ""
}
```


# sql自定义查询


## 保存查询sql接口


**接口地址**:`/reportservice/sql/create`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createTime": "",
  "createUser": "",
  "id": 0,
  "projectId": 0,
  "sqlDesc": "",
  "sqlName": "",
  "sqlText": "",
  "updateTime": "",
  "updateUser": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|sqlHistory|sql查询表|body|true|SqlHistory对象|SqlHistory对象|
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建人||false|string||
|&emsp;&emsp;id|主键||false|integer(int32)||
|&emsp;&emsp;projectId|项目id||false|integer(int32)||
|&emsp;&emsp;sqlDesc|描述||false|string||
|&emsp;&emsp;sqlName|名称||false|string||
|&emsp;&emsp;sqlText|sql内容||false|string||
|&emsp;&emsp;updateTime|最后更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«boolean»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|boolean||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": false,
	"msg": ""
}
```


## 删除查询sql接口


**接口地址**:`/reportservice/sql/delete`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createTime": "",
  "createUser": "",
  "id": 0,
  "projectId": 0,
  "sqlDesc": "",
  "sqlName": "",
  "sqlText": "",
  "updateTime": "",
  "updateUser": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|sqlHistory|sql查询表|body|true|SqlHistory对象|SqlHistory对象|
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建人||false|string||
|&emsp;&emsp;id|主键||false|integer(int32)||
|&emsp;&emsp;projectId|项目id||false|integer(int32)||
|&emsp;&emsp;sqlDesc|描述||false|string||
|&emsp;&emsp;sqlName|名称||false|string||
|&emsp;&emsp;sqlText|sql内容||false|string||
|&emsp;&emsp;updateTime|最后更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«string»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


## 查询sql详情接口


**接口地址**:`/reportservice/sql/detail`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createTime": "",
  "createUser": "",
  "id": 0,
  "projectId": 0,
  "sqlDesc": "",
  "sqlName": "",
  "sqlText": "",
  "updateTime": "",
  "updateUser": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|query|sql查询表|body|true|SqlHistory对象|SqlHistory对象|
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建人||false|string||
|&emsp;&emsp;id|主键||false|integer(int32)||
|&emsp;&emsp;projectId|项目id||false|integer(int32)||
|&emsp;&emsp;sqlDesc|描述||false|string||
|&emsp;&emsp;sqlName|名称||false|string||
|&emsp;&emsp;sqlText|sql内容||false|string||
|&emsp;&emsp;updateTime|最后更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«SqlHistory对象»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|SqlHistory对象|SqlHistory对象|
|&emsp;&emsp;createTime|创建时间|string(date-time)||
|&emsp;&emsp;createUser|创建人|string||
|&emsp;&emsp;id|主键|integer(int32)||
|&emsp;&emsp;projectId|项目id|integer(int32)||
|&emsp;&emsp;sqlDesc|描述|string||
|&emsp;&emsp;sqlName|名称|string||
|&emsp;&emsp;sqlText|sql内容|string||
|&emsp;&emsp;updateTime|最后更新时间|string(date-time)||
|&emsp;&emsp;updateUser|更新人|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"createTime": "",
		"createUser": "",
		"id": 0,
		"projectId": 0,
		"sqlDesc": "",
		"sqlName": "",
		"sqlText": "",
		"updateTime": "",
		"updateUser": ""
	},
	"msg": ""
}
```


## sql列表接口


**接口地址**:`/reportservice/sql/list`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "current": 0,
  "name": "",
  "projectId": 0,
  "size": 0
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|page|page|body|true|SqlPage|SqlPage|
|&emsp;&emsp;current|||false|integer(int64)||
|&emsp;&emsp;name|||false|string||
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;size|||false|integer(int64)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## sql查询接口


**接口地址**:`/reportservice/sql/query`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "projectId": 12,
  "sql": "select count(1) from user"
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|tmp|tmp|body|true|ProjectVo|ProjectVo|
|&emsp;&emsp;projectId|项目id||false|integer(int32)||
|&emsp;&emsp;sql|用户输入的sql||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«SqlQueryResponseVo»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|SqlQueryResponseVo|SqlQueryResponseVo|
|&emsp;&emsp;list||array|Map«string,object»|
|&emsp;&emsp;&emsp;&emsp;additionalProperty1||||
|&emsp;&emsp;message||string||
|&emsp;&emsp;sql||string||
|&emsp;&emsp;status||boolean||
|&emsp;&emsp;warn||string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"list": [
			{}
		],
		"message": "",
		"sql": "",
		"status": true,
		"warn": ""
	},
	"msg": ""
}
```


## 表列表接口


**接口地址**:`/reportservice/sql/tables`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "projectId": 12,
  "sql": "select count(1) from user"
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|tmp|tmp|body|true|ProjectVo|ProjectVo|
|&emsp;&emsp;projectId|项目id||false|integer(int32)||
|&emsp;&emsp;sql|用户输入的sql||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«SqlTableVo»»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|SqlTableVo|
|&emsp;&emsp;columns||array|EventAttr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;&emsp;&emsp;attrName|显示名称|string||
|&emsp;&emsp;&emsp;&emsp;dataType|dataType,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;hasDict|hasDict|integer||
|&emsp;&emsp;&emsp;&emsp;systemCode|doris字段|string||
|&emsp;&emsp;tableName|表名|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"columns": [
				{
					"attrCode": "",
					"attrName": "",
					"dataType": "",
					"hasDict": 0,
					"systemCode": "user_id"
				}
			],
			"tableName": "user,或者event"
		}
	],
	"msg": ""
}
```


## 更新查询sql接口


**接口地址**:`/reportservice/sql/update`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createTime": "",
  "createUser": "",
  "id": 0,
  "projectId": 0,
  "sqlDesc": "",
  "sqlName": "",
  "sqlText": "",
  "updateTime": "",
  "updateUser": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|sqlHistory|sql查询表|body|true|SqlHistory对象|SqlHistory对象|
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建人||false|string||
|&emsp;&emsp;id|主键||false|integer(int32)||
|&emsp;&emsp;projectId|项目id||false|integer(int32)||
|&emsp;&emsp;sqlDesc|描述||false|string||
|&emsp;&emsp;sqlName|名称||false|string||
|&emsp;&emsp;sqlText|sql内容||false|string||
|&emsp;&emsp;updateTime|最后更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«boolean»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|boolean||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": false,
	"msg": ""
}
```


# 字典表数据查询


## getDictionaryItem


**接口地址**:`/reportservice/dictionary-common/dicItems`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>根据type和code查询字典表数据</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|type|字典项类型|query|true|string||
|code|字典项code|query|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«DictionaryCommon»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|DictionaryCommon|
|&emsp;&emsp;code||string||
|&emsp;&emsp;id||string||
|&emsp;&emsp;status||integer(int32)||
|&emsp;&emsp;type||string||
|&emsp;&emsp;value||string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"code": "",
			"id": "",
			"status": 0,
			"type": "",
			"value": ""
		}
	],
	"msg": ""
}
```


# 用户列表


## userDataDownload


**接口地址**:`/reportservice/user/download`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>用户数据下载</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userVo|用户列表查询参数|body|true|UserVo|UserVo|
|&emsp;&emsp;condition|||false|object||
|&emsp;&emsp;current|||false|integer(int32)||
|&emsp;&emsp;fields|||false|array|string|
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;size|||false|integer(int32)||
|&emsp;&emsp;type|||false|string||
|UserVo|用户列表查询参数|body|true|UserVo|UserVo|
|&emsp;&emsp;condition|||false|object||
|&emsp;&emsp;current|||false|integer(int32)||
|&emsp;&emsp;fields|||false|array|string|
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;size|||false|integer(int32)||
|&emsp;&emsp;type|||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


暂无


**响应示例**:
```javascript

```


## userInfoTurnPage


**接口地址**:`/reportservice/user/turnPage`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>用户详情翻页</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userInfoTurnPageVo|用户详情翻页vo|body|true|String|String|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## userAttr


**接口地址**:`/reportservice/user/userAttr`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>查询用户属性</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userAttrParam|userAttrParam|body|true|||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## userInfo


**接口地址**:`/reportservice/user/userInfo`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>用户详情</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userInfoVo|userInfoVo|body|true|UserInfoVo|UserInfoVo|
|&emsp;&emsp;fields|||false|array|string|
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;userId|||false|string||
|userId|用户id|body|true|String|String|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## getUsers


**接口地址**:`/reportservice/user/users`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>查询用户列表</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userVo|用户列表查询参数|body|true|UserVo|UserVo|
|&emsp;&emsp;condition|||false|object||
|&emsp;&emsp;current|||false|integer(int32)||
|&emsp;&emsp;fields|||false|array|string|
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;size|||false|integer(int32)||
|&emsp;&emsp;type|||false|string||
|UserVo|用户列表查询参数|body|true|UserVo|UserVo|
|&emsp;&emsp;condition|||false|object||
|&emsp;&emsp;current|||false|integer(int32)||
|&emsp;&emsp;fields|||false|array|string|
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;size|||false|integer(int32)||
|&emsp;&emsp;type|||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


# 用户属性管理


## userMetas


**接口地址**:`/reportservice/user-attr/{projectId}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>分页查询用户属性</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|path|true|integer(int32)||
|attrName|属性名|query|false|string||
|dataType|数据类型|query|false|string||
|display|是否显示|query|false|integer(int32)||
|attrType|属性类型|query|false|integer(int32)||
|current|当前页|query|false|ref||
|size|每页大小|query|false|ref||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«PageResultDto«MetaDto»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|PageResultDto«MetaDto»|PageResultDto«MetaDto»|
|&emsp;&emsp;rows|数据|array|MetaDto|
|&emsp;&emsp;&emsp;&emsp;attrName|属性名|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;dimension|维度表|string||
|&emsp;&emsp;&emsp;&emsp;displayName|属性显示名|string||
|&emsp;&emsp;&emsp;&emsp;eventType|属性类型|integer||
|&emsp;&emsp;&emsp;&emsp;id|属性id|integer||
|&emsp;&emsp;&emsp;&emsp;showType|显示状态|integer||
|&emsp;&emsp;&emsp;&emsp;systemCode|属性表字段|string||
|&emsp;&emsp;&emsp;&emsp;unit|单位|string||
|&emsp;&emsp;total|总条数|integer(int64)||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"rows": [
			{
				"attrName": "",
				"dataType": "",
				"dimension": "",
				"displayName": "",
				"eventType": 0,
				"id": 0,
				"showType": 0,
				"systemCode": "",
				"unit": ""
			}
		],
		"total": 100
	},
	"msg": ""
}
```


## updateDisplayName


**接口地址**:`/reportservice/user-attr/{projectId}/{attrId}`


**请求方式**:`PATCH`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>修改用户属性显示名</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|projectId|path|true|integer(int32)||
|attrId|attrId|path|true|integer(int32)||
|displayName|displayName|query|true|string||
|userMeta|用户属性|body|true|UserMeta|UserMeta|


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|204|No Content||
|401|Unauthorized||
|403|Forbidden||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


# 标签管理


## getTagsByName


**接口地址**:`/reportservice/tags/getTagsByName`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createTime": "",
  "createUser": "",
  "id": 0,
  "parentName": "",
  "tagDesc": "",
  "tagLevel": 0,
  "tagName": "",
  "tagPath": "",
  "updateTime": "",
  "updateUser": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|tagsTemplate|标签层级模版表|body|true|TdTagsTemplate对象|TdTagsTemplate对象|
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建人||false|string||
|&emsp;&emsp;id|主键||false|integer(int32)||
|&emsp;&emsp;parentName|上层标签名称||false|string||
|&emsp;&emsp;tagDesc|sql内容||false|string||
|&emsp;&emsp;tagLevel|标签层级||false|integer(int32)||
|&emsp;&emsp;tagName|sql内容||false|string||
|&emsp;&emsp;tagPath|sql内容||false|string||
|&emsp;&emsp;updateTime|最后更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«TagsVo»»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|TagsVo|
|&emsp;&emsp;hasChilren||boolean||
|&emsp;&emsp;tagLevel||integer(int32)||
|&emsp;&emsp;tagName||string||
|&emsp;&emsp;tagPath||string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"hasChilren": true,
			"tagLevel": 0,
			"tagName": "",
			"tagPath": ""
		}
	],
	"msg": ""
}
```


## uploadFile


**接口地址**:`/reportservice/tags/uploadTemplate`


**请求方式**:`POST`


**请求数据类型**:`multipart/form-data`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|file|file|formData|true|file||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«boolean»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|boolean||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": false,
	"msg": ""
}
```


# 用户事件


## getChart


**接口地址**:`/reportservice/user/userEvent/chart`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "chartType": "",
  "current": 0,
  "dateCondition": "",
  "eventCodes": [],
  "filterEvent": "",
  "projectId": 0,
  "size": 0,
  "sort": "",
  "type": "",
  "userId": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userEventVo|用户事件详情列表查询vo|body|true|UserEventListVo|UserEventListVo|
|&emsp;&emsp;chartType|||false|string||
|&emsp;&emsp;current|||false|integer(int32)||
|&emsp;&emsp;dateCondition|||false|string||
|&emsp;&emsp;eventCodes|||false|array|string|
|&emsp;&emsp;filterEvent|||false|string||
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;size|||false|integer(int32)||
|&emsp;&emsp;sort|||false|string||
|&emsp;&emsp;type|||false|string||
|&emsp;&emsp;userId|||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## getUserEventDetail


**接口地址**:`/reportservice/user/userEvent/detail`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "chartType": "",
  "current": 0,
  "dateCondition": "",
  "eventCodes": [],
  "filterEvent": "",
  "projectId": 0,
  "size": 0,
  "sort": "",
  "type": "",
  "userId": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userEventListVo|用户事件详情列表查询vo|body|true|UserEventListVo|UserEventListVo|
|&emsp;&emsp;chartType|||false|string||
|&emsp;&emsp;current|||false|integer(int32)||
|&emsp;&emsp;dateCondition|||false|string||
|&emsp;&emsp;eventCodes|||false|array|string|
|&emsp;&emsp;filterEvent|||false|string||
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;size|||false|integer(int32)||
|&emsp;&emsp;sort|||false|string||
|&emsp;&emsp;type|||false|string||
|&emsp;&emsp;userId|||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## getEvent


**接口地址**:`/reportservice/user/userEvent/event`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|params|params|body|true|||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


# 配置信息查询


## getMenuList


**接口地址**:`/reportservice/config/menus/{appCode}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>查询菜单目录</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|appCode|产品code|path|true|string||
|umId|用户邮箱|query|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«Map«string,object»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


# 看板管理


## saveChartGroup


**接口地址**:`/reportservice/chartGroup`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>保存看板</p>



**请求示例**:


```javascript
{
  "chartGroupName": "",
  "folderId": 0,
  "projectId": 0
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|chartGroup|看板|body|true|ChartGroupVo|ChartGroupVo|
|&emsp;&emsp;chartGroupName|看板名||true|string||
|&emsp;&emsp;folderId|看板组id||false|integer(int32)||
|&emsp;&emsp;projectId|项目ID||true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## updateChartGroupName


**接口地址**:`/reportservice/chartGroup`


**请求方式**:`PATCH`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>更新看板名</p>



**请求示例**:


```javascript
{
  "chartGroupId": 0,
  "chartGroupName": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|chartGroupEditorVo|更新报表|body|true|ChartGroupEditorVo|ChartGroupEditorVo|
|&emsp;&emsp;chartGroupId|看板id||false|integer(int32)||
|&emsp;&emsp;chartGroupName|看板名||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|204|No Content||
|401|Unauthorized||
|403|Forbidden||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## saveChartGroup


**接口地址**:`/reportservice/chartGroup/folder/{newFolderId}`


**请求方式**:`PUT`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>移动看板至看板组，支持批量</p>



**请求示例**:


```javascript
[
  {
    "chartGroupIds": [],
    "oldFolderId": 0,
    "oldFolderName": 0
  }
]
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|chartGroups|chartGroups|body|true|array|MoveChartGroupVo|
|&emsp;&emsp;chartGroupIds|看板ID集合||false|array|integer|
|&emsp;&emsp;oldFolderId|旧看板组ID||false|integer(int32)||
|&emsp;&emsp;oldFolderName|旧看板组名||false|integer(int32)||
|newFolderId|newFolderId|path|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## getChartGroupDetail


**接口地址**:`/reportservice/chartGroup/{chartGroupId}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>查看看板详情</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|chartGroupId|chartGroupId|path|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«Chart»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|Chart|
|&emsp;&emsp;chartName|图表名|string||
|&emsp;&emsp;chartSize|图表尺寸，small、medium、big|string||
|&emsp;&emsp;chartSource|图表来源，事件分析：event，属性分析：userAttr，漏洞分析：vulnerability，留存分析：retention，分布分析：distribution，归因分析：attribution，间隔分析：interval，用户路径：userPath，LTV分析：ltv|string||
|&emsp;&emsp;chartType|图表类型，折线图：line，数据叠加折线图：DataOverlayLine ，饼图：pie，histogram，表格：sheet，数值类型：numerical|string||
|&emsp;&emsp;createTime|创建时间|string(date-time)||
|&emsp;&emsp;createType|创建图表方式，手动创建还是导入|integer(int32)||
|&emsp;&emsp;createUser|创建人|integer(int32)||
|&emsp;&emsp;id||integer(int32)||
|&emsp;&emsp;projectId|项目ID|integer(int32)||
|&emsp;&emsp;queryData|查询条件|string||
|&emsp;&emsp;remarks|描述|string||
|&emsp;&emsp;status|是否禁用 1:未禁用  0:禁用|integer(int32)||
|&emsp;&emsp;updateTime|更新时间|string(date-time)||
|&emsp;&emsp;updateUser|更新人|integer(int32)||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"chartName": "事件分析",
			"chartSize": "small",
			"chartSource": "event",
			"chartType": "line",
			"createTime": "2021-08-19T09:26:03.281Z",
			"createType": 1,
			"createUser": 12,
			"id": 0,
			"projectId": 12,
			"queryData": "{\"aa\":\"cc\"}",
			"remarks": "一周的用户登录数",
			"status": 1,
			"updateTime": "2021-08-19T09:26:03.281Z",
			"updateUser": 12
		}
	],
	"msg": ""
}
```


## deleteChartGroup


**接口地址**:`/reportservice/chartGroup/{chartGroupId}`


**请求方式**:`DELETE`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>删除看板</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|chartGroupId|图表id|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|204|No Content||
|401|Unauthorized||
|403|Forbidden||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## getChartGroup


**接口地址**:`/reportservice/chartGroup/{projectId}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>获取添加报表时候选择的看板列表</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|projectId|path|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«ChartGroup对象»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|ChartGroup对象|
|&emsp;&emsp;chartGroupName|看板名|string||
|&emsp;&emsp;createTime|创建时间|string(date-time)||
|&emsp;&emsp;createUser|创建人|integer(int32)||
|&emsp;&emsp;display|显示状态：0隐藏 1显示|integer(int32)||
|&emsp;&emsp;id|看板ID|integer(int32)||
|&emsp;&emsp;projectId|项目ID|integer(int32)||
|&emsp;&emsp;status|是否禁用 1:未禁用  0:禁用|integer(int32)||
|&emsp;&emsp;updateTime|更新时间|string(date-time)||
|&emsp;&emsp;updateUser|更新人|integer(int32)||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"chartGroupName": "",
			"createTime": "",
			"createUser": 0,
			"display": 0,
			"id": 0,
			"projectId": 0,
			"status": 0,
			"updateTime": "",
			"updateUser": 0
		}
	],
	"msg": ""
}
```


# 转化漏斗


## report


**接口地址**:`/reportservice/funnel/report`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "byFields": [
    {
      "attrCode": "event_time",
      "attrName": "事件触发时间",
      "attrType": "1",
      "dataType": "number",
      "intervals": "[0,100,300]"
    }
  ],
  "filter": {
    "conditions": [
      {
        "attr": {
          "attrCode": "event_time",
          "attrName": "事件触发时间",
          "attrType": "1",
          "dataType": "number",
          "intervals": "[0,100,300]"
        },
        "conditions": "[]",
        "functionAlias": "date",
        "functionCode": "EQUAL(\"等于\", \"equal\")",
        "params": "[]",
        "relation": "and",
        "renderIndex": 2
      }
    ],
    "relation": ""
  },
  "fromDate": "",
  "maxConvertTime": 0,
  "projectId": 0,
  "steps": [
    {
      "customName": "",
      "eventAlias": "",
      "eventCode": "",
      "filter": {
        "conditions": [
          {
            "attr": {
              "attrCode": "event_time",
              "attrName": "事件触发时间",
              "attrType": "1",
              "dataType": "number",
              "intervals": "[0,100,300]"
            },
            "conditions": "[]",
            "functionAlias": "date",
            "functionCode": "EQUAL(\"等于\", \"equal\")",
            "params": "[]",
            "relation": "and",
            "renderIndex": 2
          }
        ],
        "relation": ""
      }
    }
  ],
  "toDate": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|request|body|true|FunnelAnalysisRequest|FunnelAnalysisRequest|
|&emsp;&emsp;byFields|||false|array|Attr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;fromDate|开始时间||true|string(date-time)||
|&emsp;&emsp;maxConvertTime|||false|integer(int64)||
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;steps|||false|array|Step|
|&emsp;&emsp;&emsp;&emsp;customName|||false|string||
|&emsp;&emsp;&emsp;&emsp;eventAlias|||false|string||
|&emsp;&emsp;&emsp;&emsp;eventCode|||false|string||
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;toDate|终止时间||true|string(date-time)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«FunnelReportResult»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|FunnelReportResult|FunnelReportResult|
|&emsp;&emsp;byFields||array|Attr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;&emsp;&emsp;attrName|属性名|string||
|&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间|array|number|
|&emsp;&emsp;rows||array|Row|
|&emsp;&emsp;&emsp;&emsp;byValues|分组字段对应的值|array|object|
|&emsp;&emsp;&emsp;&emsp;values|该分组每个时间段的值,里面的double数组是有几个指标，数组长度就有多少|array|array|
|&emsp;&emsp;series||array|integer|
|&emsp;&emsp;stepsNames||array|string|
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"byFields": [
			{
				"attrCode": "event_time",
				"attrName": "事件触发时间",
				"attrType": "1",
				"dataType": "number",
				"intervals": "[0,100,300]"
			}
		],
		"rows": [
			{
				"byValues": [],
				"values": []
			}
		],
		"series": [],
		"stepsNames": []
	},
	"msg": ""
}
```


# 归因分析


## Analysis


**接口地址**:`/reportservice/attribution/analysis`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "allFilter": {
    "conditions": [
      {
        "attr": {
          "attrCode": "event_time",
          "attrName": "事件触发时间",
          "attrType": "1",
          "dataType": "number",
          "intervals": "[0,100,300]"
        },
        "conditions": "[]",
        "functionAlias": "date",
        "functionCode": "EQUAL(\"等于\", \"equal\")",
        "params": "[]",
        "relation": "and",
        "renderIndex": 2
      }
    ],
    "relation": ""
  },
  "byFields": [
    {
      "attrCode": "event_time",
      "attrName": "事件触发时间",
      "attrType": "1",
      "dataType": "number",
      "intervals": "[0,100,300]"
    }
  ],
  "eventSql": "",
  "fromDate": "",
  "groupBySystemFields": [],
  "modelType": "",
  "projectId": 0,
  "scrollWindow": 0,
  "targetFilter": {
    "event": [],
    "filter": {
      "conditions": [
        {
          "attr": {
            "attrCode": "event_time",
            "attrName": "事件触发时间",
            "attrType": "1",
            "dataType": "number",
            "intervals": "[0,100,300]"
          },
          "conditions": "[]",
          "functionAlias": "date",
          "functionCode": "EQUAL(\"等于\", \"equal\")",
          "params": "[]",
          "relation": "and",
          "renderIndex": 2
        }
      ],
      "relation": ""
    }
  },
  "timeUnit": "",
  "toDate": "",
  "withAttributionFilter": {
    "event": [],
    "filter": {
      "conditions": [
        {
          "attr": {
            "attrCode": "event_time",
            "attrName": "事件触发时间",
            "attrType": "1",
            "dataType": "number",
            "intervals": "[0,100,300]"
          },
          "conditions": "[]",
          "functionAlias": "date",
          "functionCode": "EQUAL(\"等于\", \"equal\")",
          "params": "[]",
          "relation": "and",
          "renderIndex": 2
        }
      ],
      "relation": ""
    }
  }
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|attributionVo|attributionVo|body|true|AttributionVo|AttributionVo|
|&emsp;&emsp;allFilter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;byFields|||false|array|Attr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;eventSql|||false|string||
|&emsp;&emsp;fromDate|||false|string(date-time)||
|&emsp;&emsp;groupBySystemFields|||false|array|string|
|&emsp;&emsp;modelType|||false|string||
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;scrollWindow|||false|integer(int32)||
|&emsp;&emsp;targetFilter|||false|AttributionIndicator|AttributionIndicator|
|&emsp;&emsp;&emsp;&emsp;event|||false|array|string|
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;timeUnit|||false|string||
|&emsp;&emsp;toDate|||false|string(date-time)||
|&emsp;&emsp;withAttributionFilter|||false|AttributionIndicator|AttributionIndicator|
|&emsp;&emsp;&emsp;&emsp;event|||false|array|string|
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## getEventAttr


**接口地址**:`/reportservice/attribution/getEventAttr`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|projectId|query|false|integer(int32)||
|eventCode|eventCode|query|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


# 项目管理


## create


**接口地址**:`/reportservice/project/create`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "appkey": "",
  "createTime": "",
  "createUser": "",
  "descText": "",
  "eventAttrCount": 0,
  "eventCount": 0,
  "eventTopicName": "",
  "id": 0,
  "isDefault": true,
  "metaEventCount": 0,
  "projectName": "",
  "status": true,
  "updateTime": "",
  "updateUser": "",
  "userAttrCount": 0,
  "userTopicName": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|project|project|body|true|Project|Project|
|&emsp;&emsp;appkey|||false|string||
|&emsp;&emsp;createTime|||false|string(date-time)||
|&emsp;&emsp;createUser|||false|string||
|&emsp;&emsp;descText|||false|string||
|&emsp;&emsp;eventAttrCount|||false|integer(int32)||
|&emsp;&emsp;eventCount|||false|integer(int64)||
|&emsp;&emsp;eventTopicName|||false|string||
|&emsp;&emsp;id|||false|integer(int32)||
|&emsp;&emsp;isDefault|||false|boolean||
|&emsp;&emsp;metaEventCount|||false|integer(int64)||
|&emsp;&emsp;projectName|||false|string||
|&emsp;&emsp;status|||false|boolean||
|&emsp;&emsp;updateTime|||false|string(date-time)||
|&emsp;&emsp;updateUser|||false|string||
|&emsp;&emsp;userAttrCount|||false|integer(int32)||
|&emsp;&emsp;userTopicName|||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«Project»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|Project|Project|
|&emsp;&emsp;appkey||string||
|&emsp;&emsp;createTime||string(date-time)||
|&emsp;&emsp;createUser||string||
|&emsp;&emsp;descText||string||
|&emsp;&emsp;eventAttrCount||integer(int32)||
|&emsp;&emsp;eventCount||integer(int64)||
|&emsp;&emsp;eventTopicName||string||
|&emsp;&emsp;id||integer(int32)||
|&emsp;&emsp;isDefault||boolean||
|&emsp;&emsp;metaEventCount||integer(int64)||
|&emsp;&emsp;projectName||string||
|&emsp;&emsp;status||boolean||
|&emsp;&emsp;updateTime||string(date-time)||
|&emsp;&emsp;updateUser||string||
|&emsp;&emsp;userAttrCount||integer(int32)||
|&emsp;&emsp;userTopicName||string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"appkey": "",
		"createTime": "",
		"createUser": "",
		"descText": "",
		"eventAttrCount": 0,
		"eventCount": 0,
		"eventTopicName": "",
		"id": 0,
		"isDefault": true,
		"metaEventCount": 0,
		"projectName": "",
		"status": true,
		"updateTime": "",
		"updateUser": "",
		"userAttrCount": 0,
		"userTopicName": ""
	},
	"msg": ""
}
```


## delete


**接口地址**:`/reportservice/project/delete`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "appkey": "",
  "createTime": "",
  "createUser": "",
  "descText": "",
  "eventAttrCount": 0,
  "eventCount": 0,
  "eventTopicName": "",
  "id": 0,
  "isDefault": true,
  "metaEventCount": 0,
  "projectName": "",
  "status": true,
  "updateTime": "",
  "updateUser": "",
  "userAttrCount": 0,
  "userTopicName": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|project|project|body|true|Project|Project|
|&emsp;&emsp;appkey|||false|string||
|&emsp;&emsp;createTime|||false|string(date-time)||
|&emsp;&emsp;createUser|||false|string||
|&emsp;&emsp;descText|||false|string||
|&emsp;&emsp;eventAttrCount|||false|integer(int32)||
|&emsp;&emsp;eventCount|||false|integer(int64)||
|&emsp;&emsp;eventTopicName|||false|string||
|&emsp;&emsp;id|||false|integer(int32)||
|&emsp;&emsp;isDefault|||false|boolean||
|&emsp;&emsp;metaEventCount|||false|integer(int64)||
|&emsp;&emsp;projectName|||false|string||
|&emsp;&emsp;status|||false|boolean||
|&emsp;&emsp;updateTime|||false|string(date-time)||
|&emsp;&emsp;updateUser|||false|string||
|&emsp;&emsp;userAttrCount|||false|integer(int32)||
|&emsp;&emsp;userTopicName|||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«string»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


## detail


**接口地址**:`/reportservice/project/detail`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "appkey": "",
  "createTime": "",
  "createUser": "",
  "descText": "",
  "eventAttrCount": 0,
  "eventCount": 0,
  "eventTopicName": "",
  "id": 0,
  "isDefault": true,
  "metaEventCount": 0,
  "projectName": "",
  "status": true,
  "updateTime": "",
  "updateUser": "",
  "userAttrCount": 0,
  "userTopicName": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|project|project|body|true|Project|Project|
|&emsp;&emsp;appkey|||false|string||
|&emsp;&emsp;createTime|||false|string(date-time)||
|&emsp;&emsp;createUser|||false|string||
|&emsp;&emsp;descText|||false|string||
|&emsp;&emsp;eventAttrCount|||false|integer(int32)||
|&emsp;&emsp;eventCount|||false|integer(int64)||
|&emsp;&emsp;eventTopicName|||false|string||
|&emsp;&emsp;id|||false|integer(int32)||
|&emsp;&emsp;isDefault|||false|boolean||
|&emsp;&emsp;metaEventCount|||false|integer(int64)||
|&emsp;&emsp;projectName|||false|string||
|&emsp;&emsp;status|||false|boolean||
|&emsp;&emsp;updateTime|||false|string(date-time)||
|&emsp;&emsp;updateUser|||false|string||
|&emsp;&emsp;userAttrCount|||false|integer(int32)||
|&emsp;&emsp;userTopicName|||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«Project»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|Project|Project|
|&emsp;&emsp;appkey||string||
|&emsp;&emsp;createTime||string(date-time)||
|&emsp;&emsp;createUser||string||
|&emsp;&emsp;descText||string||
|&emsp;&emsp;eventAttrCount||integer(int32)||
|&emsp;&emsp;eventCount||integer(int64)||
|&emsp;&emsp;eventTopicName||string||
|&emsp;&emsp;id||integer(int32)||
|&emsp;&emsp;isDefault||boolean||
|&emsp;&emsp;metaEventCount||integer(int64)||
|&emsp;&emsp;projectName||string||
|&emsp;&emsp;status||boolean||
|&emsp;&emsp;updateTime||string(date-time)||
|&emsp;&emsp;updateUser||string||
|&emsp;&emsp;userAttrCount||integer(int32)||
|&emsp;&emsp;userTopicName||string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"appkey": "",
		"createTime": "",
		"createUser": "",
		"descText": "",
		"eventAttrCount": 0,
		"eventCount": 0,
		"eventTopicName": "",
		"id": 0,
		"isDefault": true,
		"metaEventCount": 0,
		"projectName": "",
		"status": true,
		"updateTime": "",
		"updateUser": "",
		"userAttrCount": 0,
		"userTopicName": ""
	},
	"msg": ""
}
```


## list


**接口地址**:`/reportservice/project/list`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "current": 0,
  "projectId": 0,
  "projectName": "",
  "size": 0,
  "status": 0
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|page|page|body|true|ProjectPage|ProjectPage|
|&emsp;&emsp;current|||false|integer(int64)||
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;projectName|||false|string||
|&emsp;&emsp;size|||false|integer(int64)||
|&emsp;&emsp;status|||false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## update


**接口地址**:`/reportservice/project/update`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "appkey": "",
  "createTime": "",
  "createUser": "",
  "descText": "",
  "eventAttrCount": 0,
  "eventCount": 0,
  "eventTopicName": "",
  "id": 0,
  "isDefault": true,
  "metaEventCount": 0,
  "projectName": "",
  "status": true,
  "updateTime": "",
  "updateUser": "",
  "userAttrCount": 0,
  "userTopicName": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|project|project|body|true|Project|Project|
|&emsp;&emsp;appkey|||false|string||
|&emsp;&emsp;createTime|||false|string(date-time)||
|&emsp;&emsp;createUser|||false|string||
|&emsp;&emsp;descText|||false|string||
|&emsp;&emsp;eventAttrCount|||false|integer(int32)||
|&emsp;&emsp;eventCount|||false|integer(int64)||
|&emsp;&emsp;eventTopicName|||false|string||
|&emsp;&emsp;id|||false|integer(int32)||
|&emsp;&emsp;isDefault|||false|boolean||
|&emsp;&emsp;metaEventCount|||false|integer(int64)||
|&emsp;&emsp;projectName|||false|string||
|&emsp;&emsp;status|||false|boolean||
|&emsp;&emsp;updateTime|||false|string(date-time)||
|&emsp;&emsp;updateUser|||false|string||
|&emsp;&emsp;userAttrCount|||false|integer(int32)||
|&emsp;&emsp;userTopicName|||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«Project»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|Project|Project|
|&emsp;&emsp;appkey||string||
|&emsp;&emsp;createTime||string(date-time)||
|&emsp;&emsp;createUser||string||
|&emsp;&emsp;descText||string||
|&emsp;&emsp;eventAttrCount||integer(int32)||
|&emsp;&emsp;eventCount||integer(int64)||
|&emsp;&emsp;eventTopicName||string||
|&emsp;&emsp;id||integer(int32)||
|&emsp;&emsp;isDefault||boolean||
|&emsp;&emsp;metaEventCount||integer(int64)||
|&emsp;&emsp;projectName||string||
|&emsp;&emsp;status||boolean||
|&emsp;&emsp;updateTime||string(date-time)||
|&emsp;&emsp;updateUser||string||
|&emsp;&emsp;userAttrCount||integer(int32)||
|&emsp;&emsp;userTopicName||string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"appkey": "",
		"createTime": "",
		"createUser": "",
		"descText": "",
		"eventAttrCount": 0,
		"eventCount": 0,
		"eventTopicName": "",
		"id": 0,
		"isDefault": true,
		"metaEventCount": 0,
		"projectName": "",
		"status": true,
		"updateTime": "",
		"updateUser": "",
		"userAttrCount": 0,
		"userTopicName": ""
	},
	"msg": ""
}
```


# 虚拟事件


## create


**接口地址**:`/reportservice/virtualevent/create`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createTime": "",
  "createUser": "",
  "id": 0,
  "indicators": [
    {
      "aggregatorAlias": "",
      "aggregatorCode": "",
      "eventAlias": "logout",
      "eventAttr": {
        "attrCode": "event_time",
        "attrName": "事件触发时间",
        "attrType": "1",
        "dataType": "number",
        "intervals": "[0,100,300]"
      },
      "eventCode": "login",
      "filter": {
        "conditions": [
          {
            "attr": {
              "attrCode": "event_time",
              "attrName": "事件触发时间",
              "attrType": "1",
              "dataType": "number",
              "intervals": "[0,100,300]"
            },
            "conditions": "[]",
            "functionAlias": "date",
            "functionCode": "EQUAL(\"等于\", \"equal\")",
            "params": "[]",
            "relation": "and",
            "renderIndex": 2
          }
        ],
        "relation": ""
      },
      "indicatorAlias": "年龄",
      "indicatorName": "age"
    }
  ],
  "projectId": 0,
  "updateTime": "",
  "updateUser": "",
  "virtualCode": "",
  "virtualComment": "",
  "virtualName": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|virtualEventVo|虚拟事件组中事件|body|true|VirtualEvent对象|VirtualEvent对象|
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建人||false|string||
|&emsp;&emsp;id|主键||false|integer(int32)||
|&emsp;&emsp;indicators|||false|array|Indicator|
|&emsp;&emsp;&emsp;&emsp;aggregatorAlias|||false|string||
|&emsp;&emsp;&emsp;&emsp;aggregatorCode|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;eventAlias|需要分析的事件别名||true|string||
|&emsp;&emsp;&emsp;&emsp;eventAttr|事件属性||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;eventCode|需要分析的事件code||true|string||
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;indicatorAlias|指标别名||true|string||
|&emsp;&emsp;&emsp;&emsp;indicatorName|指标显示名称||true|string||
|&emsp;&emsp;projectId|虚拟事件组id||false|integer(int32)||
|&emsp;&emsp;updateTime|最后更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|string||
|&emsp;&emsp;virtualCode|关系，有三种值,null,and,or||false|string||
|&emsp;&emsp;virtualComment|||false|string||
|&emsp;&emsp;virtualName|虚拟事件名||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«boolean»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|boolean||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": false,
	"msg": ""
}
```


## delete


**接口地址**:`/reportservice/virtualevent/delete`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createTime": "",
  "createUser": "",
  "id": 0,
  "indicators": [
    {
      "aggregatorAlias": "",
      "aggregatorCode": "",
      "eventAlias": "logout",
      "eventAttr": {
        "attrCode": "event_time",
        "attrName": "事件触发时间",
        "attrType": "1",
        "dataType": "number",
        "intervals": "[0,100,300]"
      },
      "eventCode": "login",
      "filter": {
        "conditions": [
          {
            "attr": {
              "attrCode": "event_time",
              "attrName": "事件触发时间",
              "attrType": "1",
              "dataType": "number",
              "intervals": "[0,100,300]"
            },
            "conditions": "[]",
            "functionAlias": "date",
            "functionCode": "EQUAL(\"等于\", \"equal\")",
            "params": "[]",
            "relation": "and",
            "renderIndex": 2
          }
        ],
        "relation": ""
      },
      "indicatorAlias": "年龄",
      "indicatorName": "age"
    }
  ],
  "projectId": 0,
  "updateTime": "",
  "updateUser": "",
  "virtualCode": "",
  "virtualComment": "",
  "virtualName": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|virtualEvent|虚拟事件组中事件|body|true|VirtualEvent对象|VirtualEvent对象|
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建人||false|string||
|&emsp;&emsp;id|主键||false|integer(int32)||
|&emsp;&emsp;indicators|||false|array|Indicator|
|&emsp;&emsp;&emsp;&emsp;aggregatorAlias|||false|string||
|&emsp;&emsp;&emsp;&emsp;aggregatorCode|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;eventAlias|需要分析的事件别名||true|string||
|&emsp;&emsp;&emsp;&emsp;eventAttr|事件属性||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;eventCode|需要分析的事件code||true|string||
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;indicatorAlias|指标别名||true|string||
|&emsp;&emsp;&emsp;&emsp;indicatorName|指标显示名称||true|string||
|&emsp;&emsp;projectId|虚拟事件组id||false|integer(int32)||
|&emsp;&emsp;updateTime|最后更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|string||
|&emsp;&emsp;virtualCode|关系，有三种值,null,and,or||false|string||
|&emsp;&emsp;virtualComment|||false|string||
|&emsp;&emsp;virtualName|虚拟事件名||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«string»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


## detail


**接口地址**:`/reportservice/virtualevent/detail`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createTime": "",
  "createUser": "",
  "id": 0,
  "indicators": [
    {
      "aggregatorAlias": "",
      "aggregatorCode": "",
      "eventAlias": "logout",
      "eventAttr": {
        "attrCode": "event_time",
        "attrName": "事件触发时间",
        "attrType": "1",
        "dataType": "number",
        "intervals": "[0,100,300]"
      },
      "eventCode": "login",
      "filter": {
        "conditions": [
          {
            "attr": {
              "attrCode": "event_time",
              "attrName": "事件触发时间",
              "attrType": "1",
              "dataType": "number",
              "intervals": "[0,100,300]"
            },
            "conditions": "[]",
            "functionAlias": "date",
            "functionCode": "EQUAL(\"等于\", \"equal\")",
            "params": "[]",
            "relation": "and",
            "renderIndex": 2
          }
        ],
        "relation": ""
      },
      "indicatorAlias": "年龄",
      "indicatorName": "age"
    }
  ],
  "projectId": 0,
  "updateTime": "",
  "updateUser": "",
  "virtualCode": "",
  "virtualComment": "",
  "virtualName": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|virtualEvent|虚拟事件组中事件|body|true|VirtualEvent对象|VirtualEvent对象|
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建人||false|string||
|&emsp;&emsp;id|主键||false|integer(int32)||
|&emsp;&emsp;indicators|||false|array|Indicator|
|&emsp;&emsp;&emsp;&emsp;aggregatorAlias|||false|string||
|&emsp;&emsp;&emsp;&emsp;aggregatorCode|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;eventAlias|需要分析的事件别名||true|string||
|&emsp;&emsp;&emsp;&emsp;eventAttr|事件属性||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;eventCode|需要分析的事件code||true|string||
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;indicatorAlias|指标别名||true|string||
|&emsp;&emsp;&emsp;&emsp;indicatorName|指标显示名称||true|string||
|&emsp;&emsp;projectId|虚拟事件组id||false|integer(int32)||
|&emsp;&emsp;updateTime|最后更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|string||
|&emsp;&emsp;virtualCode|关系，有三种值,null,and,or||false|string||
|&emsp;&emsp;virtualComment|||false|string||
|&emsp;&emsp;virtualName|虚拟事件名||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«VirtualEvent对象»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|VirtualEvent对象|VirtualEvent对象|
|&emsp;&emsp;createTime|创建时间|string(date-time)||
|&emsp;&emsp;createUser|创建人|string||
|&emsp;&emsp;id|主键|integer(int32)||
|&emsp;&emsp;indicators||array|Indicator|
|&emsp;&emsp;&emsp;&emsp;aggregatorAlias||string||
|&emsp;&emsp;&emsp;&emsp;aggregatorCode|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL|string||
|&emsp;&emsp;&emsp;&emsp;eventAlias|需要分析的事件别名|string||
|&emsp;&emsp;&emsp;&emsp;eventAttr|事件属性|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间|array|number|
|&emsp;&emsp;&emsp;&emsp;eventCode|需要分析的事件code|string||
|&emsp;&emsp;&emsp;&emsp;filter||Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or|string||
|&emsp;&emsp;&emsp;&emsp;indicatorAlias|指标别名|string||
|&emsp;&emsp;&emsp;&emsp;indicatorName|指标显示名称|string||
|&emsp;&emsp;projectId|虚拟事件组id|integer(int32)||
|&emsp;&emsp;updateTime|最后更新时间|string(date-time)||
|&emsp;&emsp;updateUser|更新人|string||
|&emsp;&emsp;virtualCode|关系，有三种值,null,and,or|string||
|&emsp;&emsp;virtualComment||string||
|&emsp;&emsp;virtualName|虚拟事件名|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"createTime": "",
		"createUser": "",
		"id": 0,
		"indicators": [
			{
				"aggregatorAlias": "",
				"aggregatorCode": "",
				"eventAlias": "logout",
				"eventAttr": {
					"attrCode": "event_time",
					"attrName": "事件触发时间",
					"attrType": "1",
					"dataType": "number",
					"intervals": "[0,100,300]"
				},
				"eventCode": "login",
				"filter": {
					"conditions": [
						{
							"attr": {
								"attrCode": "event_time",
								"attrName": "事件触发时间",
								"attrType": "1",
								"dataType": "number",
								"intervals": "[0,100,300]"
							},
							"conditions": "[]",
							"functionAlias": "date",
							"functionCode": "EQUAL(\"等于\", \"equal\")",
							"params": "[]",
							"relation": "and",
							"renderIndex": 2
						}
					],
					"relation": ""
				},
				"indicatorAlias": "年龄",
				"indicatorName": "age"
			}
		],
		"projectId": 0,
		"updateTime": "",
		"updateUser": "",
		"virtualCode": "",
		"virtualComment": "",
		"virtualName": ""
	},
	"msg": ""
}
```


## list


**接口地址**:`/reportservice/virtualevent/list`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "current": 0,
  "projectId": 0,
  "size": 0,
  "virtualEventName": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|page|page|body|true|VirtualEventPage|VirtualEventPage|
|&emsp;&emsp;current|||false|integer(int64)||
|&emsp;&emsp;projectId|||false|integer(int32)||
|&emsp;&emsp;size|||false|integer(int64)||
|&emsp;&emsp;virtualEventName|||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## update


**接口地址**:`/reportservice/virtualevent/update`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createTime": "",
  "createUser": "",
  "id": 0,
  "indicators": [
    {
      "aggregatorAlias": "",
      "aggregatorCode": "",
      "eventAlias": "logout",
      "eventAttr": {
        "attrCode": "event_time",
        "attrName": "事件触发时间",
        "attrType": "1",
        "dataType": "number",
        "intervals": "[0,100,300]"
      },
      "eventCode": "login",
      "filter": {
        "conditions": [
          {
            "attr": {
              "attrCode": "event_time",
              "attrName": "事件触发时间",
              "attrType": "1",
              "dataType": "number",
              "intervals": "[0,100,300]"
            },
            "conditions": "[]",
            "functionAlias": "date",
            "functionCode": "EQUAL(\"等于\", \"equal\")",
            "params": "[]",
            "relation": "and",
            "renderIndex": 2
          }
        ],
        "relation": ""
      },
      "indicatorAlias": "年龄",
      "indicatorName": "age"
    }
  ],
  "projectId": 0,
  "updateTime": "",
  "updateUser": "",
  "virtualCode": "",
  "virtualComment": "",
  "virtualName": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|virtualEventVo|虚拟事件组中事件|body|true|VirtualEvent对象|VirtualEvent对象|
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建人||false|string||
|&emsp;&emsp;id|主键||false|integer(int32)||
|&emsp;&emsp;indicators|||false|array|Indicator|
|&emsp;&emsp;&emsp;&emsp;aggregatorAlias|||false|string||
|&emsp;&emsp;&emsp;&emsp;aggregatorCode|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;eventAlias|需要分析的事件别名||true|string||
|&emsp;&emsp;&emsp;&emsp;eventAttr|事件属性||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;eventCode|需要分析的事件code||true|string||
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;indicatorAlias|指标别名||true|string||
|&emsp;&emsp;&emsp;&emsp;indicatorName|指标显示名称||true|string||
|&emsp;&emsp;projectId|虚拟事件组id||false|integer(int32)||
|&emsp;&emsp;updateTime|最后更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|string||
|&emsp;&emsp;virtualCode|关系，有三种值,null,and,or||false|string||
|&emsp;&emsp;virtualComment|||false|string||
|&emsp;&emsp;virtualName|虚拟事件名||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«boolean»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|boolean||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": false,
	"msg": ""
}
```


# 用户分群


## calculation


**接口地址**:`/reportservice/userCrowd/calculation`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "calTime": "",
  "createTime": "",
  "createUser": "",
  "crowdName": "",
  "crowdResultPath": "",
  "crowdRule": "",
  "crowdRules": {
    "allRelation": "",
    "analysisDimension": "",
    "eventCrowds": [
      {
        "aggregatorAlias": "",
        "aggregatorCode": "",
        "eventAlias": "logout",
        "eventAttr": {
          "attrCode": "event_time",
          "attrName": "事件触发时间",
          "attrType": "1",
          "dataType": "number",
          "intervals": "[0,100,300]"
        },
        "eventCode": "login",
        "filter": {
          "conditions": [
            {
              "attr": {
                "attrCode": "event_time",
                "attrName": "事件触发时间",
                "attrType": "1",
                "dataType": "number",
                "intervals": "[0,100,300]"
              },
              "conditions": "[]",
              "functionAlias": "date",
              "functionCode": "EQUAL(\"等于\", \"equal\")",
              "params": "[]",
              "relation": "and",
              "renderIndex": 2
            }
          ],
          "relation": ""
        },
        "fromDate": "",
        "indicatorAlias": "年龄",
        "indicatorName": "age",
        "toDate": ""
      }
    ],
    "eventRelation": "",
    "userAttrFilter": [
      {
        "conditions": [
          {
            "attr": {
              "attrCode": "event_time",
              "attrName": "事件触发时间",
              "attrType": "1",
              "dataType": "number",
              "intervals": "[0,100,300]"
            },
            "conditions": "[]",
            "functionAlias": "date",
            "functionCode": "EQUAL(\"等于\", \"equal\")",
            "params": "[]",
            "relation": "and",
            "renderIndex": 2
          }
        ],
        "relation": ""
      }
    ],
    "userAttrRelation": ""
  },
  "crowdType": "",
  "id": 0,
  "llContain": 0,
  "llCrowdId": 0,
  "llMultiple": 0,
  "llType": 0,
  "projectId": 0,
  "samplingFactor": 0,
  "showName": "",
  "status": 0,
  "updateTime": "",
  "updateType": 0,
  "updateUser": "",
  "userCount": 0
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userCrowdInfoVo|用户分群规则配置表|body|true|UserCrowdInfo对象|UserCrowdInfo对象|
|&emsp;&emsp;calTime|计算时间||false|string(date-time)||
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建用户||false|string||
|&emsp;&emsp;crowdName|分群名||false|string||
|&emsp;&emsp;crowdResultPath|||false|string||
|&emsp;&emsp;crowdRule|分群规则 包含属性、事件、有序事件||false|string||
|&emsp;&emsp;crowdRules|||false|CrowdRule|CrowdRule|
|&emsp;&emsp;&emsp;&emsp;allRelation|可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;analysisDimension|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;eventCrowds|||false|array|EventCrowd|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;aggregatorAlias|||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;aggregatorCode|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;eventAlias|需要分析的事件别名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;eventAttr|事件属性||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;eventCode|需要分析的事件code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;fromDate|||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;indicatorAlias|指标别名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;indicatorName|指标显示名称||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;toDate|||false|string||
|&emsp;&emsp;&emsp;&emsp;eventRelation|可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;userAttrFilter|||false|array|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;userAttrRelation|可用值:and,or||false|string||
|&emsp;&emsp;crowdType|分群类型 condition：条件分群 analyResult：分析结果保存 idUpload：id上传 lookalike :lookalike分群||false|string||
|&emsp;&emsp;id|||false|integer(int32)||
|&emsp;&emsp;llContain|||false|integer(int32)||
|&emsp;&emsp;llCrowdId|||false|integer(int32)||
|&emsp;&emsp;llMultiple|||false|integer(int32)||
|&emsp;&emsp;llType|||false|integer(int32)||
|&emsp;&emsp;projectId|项目id||false|integer(int32)||
|&emsp;&emsp;samplingFactor|抽样因子 64为全量 32为1/2||false|integer(int32)||
|&emsp;&emsp;showName|显示名||false|string||
|&emsp;&emsp;status|状态 1可用 2禁用||false|integer(int32)||
|&emsp;&emsp;updateTime|修改时间||false|string(date-time)||
|&emsp;&emsp;updateType|1 自动 2手动||false|integer(int32)||
|&emsp;&emsp;updateUser|更新用户||false|string||
|&emsp;&emsp;userCount|用户数||false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## getUserCrowdInfo


**接口地址**:`/reportservice/userCrowd/getUserCrowdInfo`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userCrowdId|userCrowdId|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## getUserCrowdInfoList


**接口地址**:`/reportservice/userCrowd/getUserCrowdInfoList`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "createUser": "",
  "crowdType": "",
  "current": 0,
  "display": 0,
  "name": "",
  "projectId": 0,
  "size": 0,
  "status": 0,
  "updateType": 0
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userCrowdVo|用户分群列表查询vo|body|true|UserCrowdVo|UserCrowdVo|
|&emsp;&emsp;createUser|创建人||false|string||
|&emsp;&emsp;crowdType|||false|string||
|&emsp;&emsp;current|当前页||true|integer(int64)||
|&emsp;&emsp;display|是否显示||false|integer(int32)||
|&emsp;&emsp;name|||false|string||
|&emsp;&emsp;projectId|应用id||true|integer(int32)||
|&emsp;&emsp;size|每页大小||true|integer(int64)||
|&emsp;&emsp;status|是否启用||false|integer(int32)||
|&emsp;&emsp;updateType|||false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## lookLikeCrowd


**接口地址**:`/reportservice/userCrowd/lookLikeCrowd`


**请求方式**:`POST`


**请求数据类型**:`multipart/form-data`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|file|file|formData|false|file||
|projectId|projectId|query|false|integer(int32)||
|crowdName|crowdName|query|false|string||
|ll_type|ll_type|query|false|integer(int32)||
|llCrowdId|llCrowdId|query|false|integer(int32)||
|llContain|llContain|query|false|integer(int32)||
|llMultiple|llMultiple|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## saveOrUpdate


**接口地址**:`/reportservice/userCrowd/save`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "calTime": "",
  "createTime": "",
  "createUser": "",
  "crowdName": "",
  "crowdResultPath": "",
  "crowdRule": "",
  "crowdRules": {
    "allRelation": "",
    "analysisDimension": "",
    "eventCrowds": [
      {
        "aggregatorAlias": "",
        "aggregatorCode": "",
        "eventAlias": "logout",
        "eventAttr": {
          "attrCode": "event_time",
          "attrName": "事件触发时间",
          "attrType": "1",
          "dataType": "number",
          "intervals": "[0,100,300]"
        },
        "eventCode": "login",
        "filter": {
          "conditions": [
            {
              "attr": {
                "attrCode": "event_time",
                "attrName": "事件触发时间",
                "attrType": "1",
                "dataType": "number",
                "intervals": "[0,100,300]"
              },
              "conditions": "[]",
              "functionAlias": "date",
              "functionCode": "EQUAL(\"等于\", \"equal\")",
              "params": "[]",
              "relation": "and",
              "renderIndex": 2
            }
          ],
          "relation": ""
        },
        "fromDate": "",
        "indicatorAlias": "年龄",
        "indicatorName": "age",
        "toDate": ""
      }
    ],
    "eventRelation": "",
    "userAttrFilter": [
      {
        "conditions": [
          {
            "attr": {
              "attrCode": "event_time",
              "attrName": "事件触发时间",
              "attrType": "1",
              "dataType": "number",
              "intervals": "[0,100,300]"
            },
            "conditions": "[]",
            "functionAlias": "date",
            "functionCode": "EQUAL(\"等于\", \"equal\")",
            "params": "[]",
            "relation": "and",
            "renderIndex": 2
          }
        ],
        "relation": ""
      }
    ],
    "userAttrRelation": ""
  },
  "crowdType": "",
  "id": 0,
  "llContain": 0,
  "llCrowdId": 0,
  "llMultiple": 0,
  "llType": 0,
  "projectId": 0,
  "samplingFactor": 0,
  "showName": "",
  "status": 0,
  "updateTime": "",
  "updateType": 0,
  "updateUser": "",
  "userCount": 0
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userCrowdInfo|用户分群规则配置表|body|true|UserCrowdInfo对象|UserCrowdInfo对象|
|&emsp;&emsp;calTime|计算时间||false|string(date-time)||
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建用户||false|string||
|&emsp;&emsp;crowdName|分群名||false|string||
|&emsp;&emsp;crowdResultPath|||false|string||
|&emsp;&emsp;crowdRule|分群规则 包含属性、事件、有序事件||false|string||
|&emsp;&emsp;crowdRules|||false|CrowdRule|CrowdRule|
|&emsp;&emsp;&emsp;&emsp;allRelation|可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;analysisDimension|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;eventCrowds|||false|array|EventCrowd|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;aggregatorAlias|||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;aggregatorCode|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;eventAlias|需要分析的事件别名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;eventAttr|事件属性||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;eventCode|需要分析的事件code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;fromDate|||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;indicatorAlias|指标别名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;indicatorName|指标显示名称||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;toDate|||false|string||
|&emsp;&emsp;&emsp;&emsp;eventRelation|可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;userAttrFilter|||false|array|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;userAttrRelation|可用值:and,or||false|string||
|&emsp;&emsp;crowdType|分群类型 condition：条件分群 analyResult：分析结果保存 idUpload：id上传 lookalike :lookalike分群||false|string||
|&emsp;&emsp;id|||false|integer(int32)||
|&emsp;&emsp;llContain|||false|integer(int32)||
|&emsp;&emsp;llCrowdId|||false|integer(int32)||
|&emsp;&emsp;llMultiple|||false|integer(int32)||
|&emsp;&emsp;llType|||false|integer(int32)||
|&emsp;&emsp;projectId|项目id||false|integer(int32)||
|&emsp;&emsp;samplingFactor|抽样因子 64为全量 32为1/2||false|integer(int32)||
|&emsp;&emsp;showName|显示名||false|string||
|&emsp;&emsp;status|状态 1可用 2禁用||false|integer(int32)||
|&emsp;&emsp;updateTime|修改时间||false|string(date-time)||
|&emsp;&emsp;updateType|1 自动 2手动||false|integer(int32)||
|&emsp;&emsp;updateUser|更新用户||false|string||
|&emsp;&emsp;userCount|用户数||false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## saveUserCrowd


**接口地址**:`/reportservice/userCrowd/update`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "calTime": "",
  "createTime": "",
  "createUser": "",
  "crowdName": "",
  "crowdResultPath": "",
  "crowdRule": "",
  "crowdRules": {
    "allRelation": "",
    "analysisDimension": "",
    "eventCrowds": [
      {
        "aggregatorAlias": "",
        "aggregatorCode": "",
        "eventAlias": "logout",
        "eventAttr": {
          "attrCode": "event_time",
          "attrName": "事件触发时间",
          "attrType": "1",
          "dataType": "number",
          "intervals": "[0,100,300]"
        },
        "eventCode": "login",
        "filter": {
          "conditions": [
            {
              "attr": {
                "attrCode": "event_time",
                "attrName": "事件触发时间",
                "attrType": "1",
                "dataType": "number",
                "intervals": "[0,100,300]"
              },
              "conditions": "[]",
              "functionAlias": "date",
              "functionCode": "EQUAL(\"等于\", \"equal\")",
              "params": "[]",
              "relation": "and",
              "renderIndex": 2
            }
          ],
          "relation": ""
        },
        "fromDate": "",
        "indicatorAlias": "年龄",
        "indicatorName": "age",
        "toDate": ""
      }
    ],
    "eventRelation": "",
    "userAttrFilter": [
      {
        "conditions": [
          {
            "attr": {
              "attrCode": "event_time",
              "attrName": "事件触发时间",
              "attrType": "1",
              "dataType": "number",
              "intervals": "[0,100,300]"
            },
            "conditions": "[]",
            "functionAlias": "date",
            "functionCode": "EQUAL(\"等于\", \"equal\")",
            "params": "[]",
            "relation": "and",
            "renderIndex": 2
          }
        ],
        "relation": ""
      }
    ],
    "userAttrRelation": ""
  },
  "crowdType": "",
  "id": 0,
  "llContain": 0,
  "llCrowdId": 0,
  "llMultiple": 0,
  "llType": 0,
  "projectId": 0,
  "samplingFactor": 0,
  "showName": "",
  "status": 0,
  "updateTime": "",
  "updateType": 0,
  "updateUser": "",
  "userCount": 0
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userCrowdInfo|用户分群规则配置表|body|true|UserCrowdInfo对象|UserCrowdInfo对象|
|&emsp;&emsp;calTime|计算时间||false|string(date-time)||
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createUser|创建用户||false|string||
|&emsp;&emsp;crowdName|分群名||false|string||
|&emsp;&emsp;crowdResultPath|||false|string||
|&emsp;&emsp;crowdRule|分群规则 包含属性、事件、有序事件||false|string||
|&emsp;&emsp;crowdRules|||false|CrowdRule|CrowdRule|
|&emsp;&emsp;&emsp;&emsp;allRelation|可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;analysisDimension|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;eventCrowds|||false|array|EventCrowd|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;aggregatorAlias|||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;aggregatorCode|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;eventAlias|需要分析的事件别名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;eventAttr|事件属性||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;eventCode|需要分析的事件code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;fromDate|||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;indicatorAlias|指标别名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;indicatorName|指标显示名称||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;toDate|||false|string||
|&emsp;&emsp;&emsp;&emsp;eventRelation|可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;userAttrFilter|||false|array|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;userAttrRelation|可用值:and,or||false|string||
|&emsp;&emsp;crowdType|分群类型 condition：条件分群 analyResult：分析结果保存 idUpload：id上传 lookalike :lookalike分群||false|string||
|&emsp;&emsp;id|||false|integer(int32)||
|&emsp;&emsp;llContain|||false|integer(int32)||
|&emsp;&emsp;llCrowdId|||false|integer(int32)||
|&emsp;&emsp;llMultiple|||false|integer(int32)||
|&emsp;&emsp;llType|||false|integer(int32)||
|&emsp;&emsp;projectId|项目id||false|integer(int32)||
|&emsp;&emsp;samplingFactor|抽样因子 64为全量 32为1/2||false|integer(int32)||
|&emsp;&emsp;showName|显示名||false|string||
|&emsp;&emsp;status|状态 1可用 2禁用||false|integer(int32)||
|&emsp;&emsp;updateTime|修改时间||false|string(date-time)||
|&emsp;&emsp;updateType|1 自动 2手动||false|integer(int32)||
|&emsp;&emsp;updateUser|更新用户||false|string||
|&emsp;&emsp;userCount|用户数||false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## uploadIdCrowd


**接口地址**:`/reportservice/userCrowd/uploadIdCrowd`


**请求方式**:`POST`


**请求数据类型**:`multipart/form-data`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|uploadFile|uploadFile|formData|false|file||
|crowdId|crowdId|query|false|integer(int32)||
|projectId|projectId|query|false|integer(int32)||
|crowdName|crowdName|query|false|string||
|joinField|joinField|query|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


# 属性分析


## 用户属性分析


**接口地址**:`/reportservice/user-analysis`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "byFields": "age",
  "current": 1,
  "filter": {
    "conditions": [
      {
        "attr": {
          "attrCode": "event_time",
          "attrName": "事件触发时间",
          "attrType": "1",
          "dataType": "number",
          "intervals": "[0,100,300]"
        },
        "conditions": "[]",
        "functionAlias": "date",
        "functionCode": "EQUAL(\"等于\", \"equal\")",
        "params": "[]",
        "relation": "and",
        "renderIndex": 2
      }
    ],
    "relation": ""
  },
  "fromDate": "2021-08-20T09:26:03.281Z",
  "indicators": [
    {
      "aggregatorAlias": "",
      "aggregatorCode": "",
      "eventAlias": "logout",
      "eventAttr": {
        "attrCode": "event_time",
        "attrName": "事件触发时间",
        "attrType": "1",
        "dataType": "number",
        "intervals": "[0,100,300]"
      },
      "eventCode": "login",
      "filter": {
        "conditions": [
          {
            "attr": {
              "attrCode": "event_time",
              "attrName": "事件触发时间",
              "attrType": "1",
              "dataType": "number",
              "intervals": "[0,100,300]"
            },
            "conditions": "[]",
            "functionAlias": "date",
            "functionCode": "EQUAL(\"等于\", \"equal\")",
            "params": "[]",
            "relation": "and",
            "renderIndex": 2
          }
        ],
        "relation": ""
      },
      "indicatorAlias": "年龄",
      "indicatorName": "age"
    }
  ],
  "projectId": 101,
  "size": 10,
  "toDate": "2021-08-19T09:26:03.281Z",
  "unit": "week"
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userAnalysis|用户分析请求|body|true|UserAnalysisRequest|UserAnalysisRequest|
|&emsp;&emsp;byFields|分组字段||false|array|Attr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;current|当前页号||false|integer(int32)||
|&emsp;&emsp;filter|filter||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;fromDate|开始时间||true|string(date-time)||
|&emsp;&emsp;indicators|指标||true|array|Indicator|
|&emsp;&emsp;&emsp;&emsp;aggregatorAlias|||false|string||
|&emsp;&emsp;&emsp;&emsp;aggregatorCode|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;eventAlias|需要分析的事件别名||true|string||
|&emsp;&emsp;&emsp;&emsp;eventAttr|事件属性||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;eventCode|需要分析的事件code||true|string||
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;indicatorAlias|指标别名||true|string||
|&emsp;&emsp;&emsp;&emsp;indicatorName|指标显示名称||true|string||
|&emsp;&emsp;projectId|应用id||true|integer(int32)||
|&emsp;&emsp;size|每页大小||false|integer(int32)||
|&emsp;&emsp;toDate|终止时间||true|string(date-time)||
|&emsp;&emsp;unit|分割单位,可用值:MONTH,WEEK,DAY,HOUR,MINUTE||true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«DetailResult»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|DetailResult|DetailResult|
|&emsp;&emsp;byFields|分组字段|array|Attr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;&emsp;&emsp;attrName|属性名|string||
|&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间|array|number|
|&emsp;&emsp;indicatorNames|指标名称|array|string|
|&emsp;&emsp;rows|数据|array|Row|
|&emsp;&emsp;&emsp;&emsp;byValues|分组字段对应的值|array|object|
|&emsp;&emsp;&emsp;&emsp;values|该分组每个时间段的值,里面的double数组是有几个指标，数组长度就有多少|array|array|
|&emsp;&emsp;series|横坐标|array|integer|
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"byFields": "age",
		"indicatorNames": "name",
		"rows": [
			{
				"byValues": [],
				"values": []
			}
		],
		"series": "100"
	},
	"msg": ""
}
```


## 根据产品id查询用户属性


**接口地址**:`/reportservice/user-analysis/userAttr`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|query|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«UserAttr»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|UserAttr|
|&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;attrName|显示名称|string||
|&emsp;&emsp;createTime||string(date-time)||
|&emsp;&emsp;createUser||string||
|&emsp;&emsp;dataType|dataType,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;dbType|dbType|string||
|&emsp;&emsp;display|display|integer(int32)||
|&emsp;&emsp;hasDict|hasDict|integer(int32)||
|&emsp;&emsp;id||integer(int32)||
|&emsp;&emsp;isCommon||integer(int32)||
|&emsp;&emsp;isNull|isNull|integer(int32)||
|&emsp;&emsp;projectId|应用id|integer(int32)||
|&emsp;&emsp;sorting|sorting|integer(int32)||
|&emsp;&emsp;status|status|integer(int32)||
|&emsp;&emsp;systemCode||string||
|&emsp;&emsp;updateTime||string(date-time)||
|&emsp;&emsp;updateUser||string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"attrCode": "sex",
			"attrName": "性别",
			"createTime": "",
			"createUser": "",
			"dataType": "bool",
			"dbType": "number",
			"display": 1,
			"hasDict": 0,
			"id": 0,
			"isCommon": 0,
			"isNull": 0,
			"projectId": 101,
			"sorting": 10,
			"status": 1,
			"systemCode": "",
			"updateTime": "",
			"updateUser": ""
		}
	],
	"msg": ""
}
```


## 根据产品id获取查询维度


**接口地址**:`/reportservice/user-analysis/userProperties`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|query|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«UserAttrVo»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|UserAttrVo|
|&emsp;&emsp;analysis|对应的聚合类型|array|string|
|&emsp;&emsp;attrCode|属性code|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"analysis": "{aggregatorCode: \"general\", aggregatorAlias: \"总次数\"}",
			"attrCode": "age"
		}
	],
	"msg": ""
}
```


# 报表管理


## 根据id查询图表


**接口地址**:`/reportservice/chart`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|query|true|integer(int32)||
|chartId|报表Id|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«Chart»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|Chart|Chart|
|&emsp;&emsp;chartName|图表名|string||
|&emsp;&emsp;chartSize|图表尺寸，small、medium、big|string||
|&emsp;&emsp;chartSource|图表来源，事件分析：event，属性分析：userAttr，漏洞分析：vulnerability，留存分析：retention，分布分析：distribution，归因分析：attribution，间隔分析：interval，用户路径：userPath，LTV分析：ltv|string||
|&emsp;&emsp;chartType|图表类型，折线图：line，数据叠加折线图：DataOverlayLine ，饼图：pie，histogram，表格：sheet，数值类型：numerical|string||
|&emsp;&emsp;createTime|创建时间|string(date-time)||
|&emsp;&emsp;createType|创建图表方式，手动创建还是导入|integer(int32)||
|&emsp;&emsp;createUser|创建人|integer(int32)||
|&emsp;&emsp;id||integer(int32)||
|&emsp;&emsp;projectId|项目ID|integer(int32)||
|&emsp;&emsp;queryData|查询条件|string||
|&emsp;&emsp;remarks|描述|string||
|&emsp;&emsp;status|是否禁用 1:未禁用  0:禁用|integer(int32)||
|&emsp;&emsp;updateTime|更新时间|string(date-time)||
|&emsp;&emsp;updateUser|更新人|integer(int32)||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"chartName": "事件分析",
		"chartSize": "small",
		"chartSource": "event",
		"chartType": "line",
		"createTime": "2021-08-19T09:26:03.281Z",
		"createType": 1,
		"createUser": 12,
		"id": 0,
		"projectId": 12,
		"queryData": "{\"aa\":\"cc\"}",
		"remarks": "一周的用户登录数",
		"status": 1,
		"updateTime": "2021-08-19T09:26:03.281Z",
		"updateUser": 12
	},
	"msg": ""
}
```


## 保存图表


**接口地址**:`/reportservice/chart`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "chartName": "事件分析",
  "chartSize": "small",
  "chartSource": "event",
  "chartType": "line",
  "createTime": "2021-08-19T09:26:03.281Z",
  "createType": 1,
  "createUser": 12,
  "id": 0,
  "projectId": 12,
  "queryData": "{\"aa\":\"cc\"}",
  "remarks": "一周的用户登录数",
  "status": 1,
  "updateTime": "2021-08-19T09:26:03.281Z",
  "updateUser": 12
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|chart|chart|body|true|Chart|Chart|
|&emsp;&emsp;chartName|图表名||true|string||
|&emsp;&emsp;chartSize|图表尺寸，small、medium、big||true|string||
|&emsp;&emsp;chartSource|图表来源，事件分析：event，属性分析：userAttr，漏洞分析：vulnerability，留存分析：retention，分布分析：distribution，归因分析：attribution，间隔分析：interval，用户路径：userPath，LTV分析：ltv||true|string||
|&emsp;&emsp;chartType|图表类型，折线图：line，数据叠加折线图：DataOverlayLine ，饼图：pie，histogram，表格：sheet，数值类型：numerical||true|string||
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createType|创建图表方式，手动创建还是导入||true|integer(int32)||
|&emsp;&emsp;createUser|创建人||false|integer(int32)||
|&emsp;&emsp;id|||false|integer(int32)||
|&emsp;&emsp;projectId|项目ID||true|integer(int32)||
|&emsp;&emsp;queryData|查询条件||true|string||
|&emsp;&emsp;remarks|描述||false|string||
|&emsp;&emsp;status|是否禁用 1:未禁用  0:禁用||true|integer(int32)||
|&emsp;&emsp;updateTime|更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## 修改报表名称或者备注


**接口地址**:`/reportservice/chart`


**请求方式**:`PATCH`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "chartId": 0,
  "chartName": "",
  "remark": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|chart|更新报表|body|true|ChartEditorVo|ChartEditorVo|
|&emsp;&emsp;chartId|报表id||false|integer(int32)||
|&emsp;&emsp;chartName|报表名称||false|string||
|&emsp;&emsp;remark|备注||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|204|No Content||
|401|Unauthorized||
|403|Forbidden||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## 保存图表至看板


**接口地址**:`/reportservice/chart/{chartGroupId}`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "chartName": "事件分析",
  "chartSize": "small",
  "chartSource": "event",
  "chartType": "line",
  "createTime": "2021-08-19T09:26:03.281Z",
  "createType": 1,
  "createUser": 12,
  "id": 0,
  "projectId": 12,
  "queryData": "{\"aa\":\"cc\"}",
  "remarks": "一周的用户登录数",
  "status": 1,
  "updateTime": "2021-08-19T09:26:03.281Z",
  "updateUser": 12
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|chart|chart|body|true|Chart|Chart|
|&emsp;&emsp;chartName|图表名||true|string||
|&emsp;&emsp;chartSize|图表尺寸，small、medium、big||true|string||
|&emsp;&emsp;chartSource|图表来源，事件分析：event，属性分析：userAttr，漏洞分析：vulnerability，留存分析：retention，分布分析：distribution，归因分析：attribution，间隔分析：interval，用户路径：userPath，LTV分析：ltv||true|string||
|&emsp;&emsp;chartType|图表类型，折线图：line，数据叠加折线图：DataOverlayLine ，饼图：pie，histogram，表格：sheet，数值类型：numerical||true|string||
|&emsp;&emsp;createTime|创建时间||false|string(date-time)||
|&emsp;&emsp;createType|创建图表方式，手动创建还是导入||true|integer(int32)||
|&emsp;&emsp;createUser|创建人||false|integer(int32)||
|&emsp;&emsp;id|||false|integer(int32)||
|&emsp;&emsp;projectId|项目ID||true|integer(int32)||
|&emsp;&emsp;queryData|查询条件||true|string||
|&emsp;&emsp;remarks|描述||false|string||
|&emsp;&emsp;status|是否禁用 1:未禁用  0:禁用||true|integer(int32)||
|&emsp;&emsp;updateTime|更新时间||false|string(date-time)||
|&emsp;&emsp;updateUser|更新人||false|integer(int32)||
|chartGroupId|看板Id|query|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## 删除报表


**接口地址**:`/reportservice/chart/{chartIds}`


**请求方式**:`DELETE`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|chartIds|图表id|path|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|204|No Content||
|401|Unauthorized||
|403|Forbidden||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## 分页查询图表-有改动


**接口地址**:`/reportservice/chart/{projectId}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|path|true|integer(int32)||
|chartName|报表名|query|false|string||
|current|当前页|query|false|integer(int32)||
|size|每页大小|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«PageResultDto«List«ChartManagerDto»»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|PageResultDto«List«ChartManagerDto»»|PageResultDto«List«ChartManagerDto»»|
|&emsp;&emsp;rows|数据|array|ChartManagerDto|
|&emsp;&emsp;&emsp;&emsp;applyCharGroup|已应用看板名|array|string|
|&emsp;&emsp;&emsp;&emsp;applySum|应用看板数|integer||
|&emsp;&emsp;&emsp;&emsp;chartId|报表id|integer||
|&emsp;&emsp;&emsp;&emsp;chartName|报表名|string||
|&emsp;&emsp;&emsp;&emsp;chartSource|报表类型 事件分析  属性分析 ...|string||
|&emsp;&emsp;&emsp;&emsp;createType|创建方式|integer||
|&emsp;&emsp;&emsp;&emsp;updateTime|更新时间|string||
|&emsp;&emsp;total|总条数|integer(int64)||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"rows": [
			{
				"applyCharGroup": "每日登录数",
				"applySum": 12,
				"chartId": 12,
				"chartName": "事件分析",
				"chartSource": "event",
				"createType": 1,
				"updateTime": "2021-08-19T09:26:03.281Z"
			}
		],
		"total": 100
	},
	"msg": ""
}
```


## 分组查询报表


**接口地址**:`/reportservice/chart/{projectId}/{chartSource}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|projectId|path|true|integer(int32)||
|chartSource|chartSource|path|true|string||
|chartName|chartName|query|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«Map«string,object»»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|Map«string,object»|
|&emsp;&emsp;additionalProperty1||||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{}
	],
	"msg": ""
}
```


# 事件管理及分析


## 获取某属性可能的取值列表


**接口地址**:`/reportservice/event/attr-values`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>获取某属性可能的取值，支持模糊搜索</p>



**请求示例**:


```javascript
{
  "projectId": 0,
  "attrType": "",
  "attrCode": "",
  "keywords": "",
  "size": 0
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|request|body|true|AttrValuesRequest|AttrValuesRequest|
|&emsp;&emsp;projectId|projectId||true|integer(int32)||
|&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;attrCode|属性attrCode||true|string||
|&emsp;&emsp;keywords|模糊搜索关键字||false|string||
|&emsp;&emsp;size|返回数据数量，默认为20||false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|获取成功|Result«List«string»»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [],
	"msg": ""
}
```


## 获取事件属性


**接口地址**:`/reportservice/event/event-attr`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>获取单个事件的属性列表</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|项目id|query|true|integer(int32)||
|eventCode|事件Code|query|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«EventAttr»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|EventAttr|
|&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;attrName|显示名称|string||
|&emsp;&emsp;dataType|dataType,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;hasDict|hasDict|integer(int32)||
|&emsp;&emsp;systemCode|doris字段|string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"attrCode": "",
			"attrName": "",
			"dataType": "",
			"hasDict": 0,
			"systemCode": "user_id"
		}
	],
	"msg": ""
}
```


## eventGroups


**接口地址**:`/reportservice/event/eventGroups`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>根据产品id查询该产品下所有的事件组以及事件组中的事件,用于分析模型中分析指标下拉框数据</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|query|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«EventGroupVo»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array|EventGroupVo|
|&emsp;&emsp;events||array|Event|
|&emsp;&emsp;&emsp;&emsp;eventAlias|事件别名|string||
|&emsp;&emsp;&emsp;&emsp;eventCode|事件code|string||
|&emsp;&emsp;&emsp;&emsp;id|事件id|integer||
|&emsp;&emsp;&emsp;&emsp;isPreset||integer||
|&emsp;&emsp;&emsp;&emsp;projectId|项目id|integer||
|&emsp;&emsp;&emsp;&emsp;remark|备注|string||
|&emsp;&emsp;groupName||string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"events": [
				{
					"eventAlias": "",
					"eventCode": "",
					"id": 0,
					"isPreset": 0,
					"projectId": 0,
					"remark": ""
				}
			],
			"groupName": ""
		}
	],
	"msg": ""
}
```


## eventProperties


**接口地址**:`/reportservice/event/eventProperties`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>根据产品id与事件名称查询该产品下对应事件的属性及查询维度</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|query|true|integer(int32)||
|eventCode|事件code|query|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«PropQuotas»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|PropQuotas|PropQuotas|
|&emsp;&emsp;props||array|EventAttrVo|
|&emsp;&emsp;&emsp;&emsp;analysis||array|string|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;&emsp;&emsp;attrName|显示名称|string||
|&emsp;&emsp;&emsp;&emsp;dataType|dataType,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;hasDict|hasDict|integer||
|&emsp;&emsp;&emsp;&emsp;systemCode|doris字段|string||
|&emsp;&emsp;staidQuots||array|string|
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"props": [
			{
				"analysis": [],
				"attrCode": "",
				"attrName": "",
				"dataType": "",
				"hasDict": 0,
				"systemCode": "user_id"
			}
		],
		"staidQuots": []
	},
	"msg": ""
}
```


## selectEventMeta


**接口地址**:`/reportservice/event/events/{projectId}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>查询事件</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|path|true|string||
|display|是否显示|query|false|integer(int32)||
|eventName|事件名|query|false|string||
|eventType|事件类型|query|false|integer(int32)||
|current|当前页|query|false|integer(int32)||
|size|每页大小|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«PageResultDto«EventDto»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|PageResultDto«EventDto»|PageResultDto«EventDto»|
|&emsp;&emsp;rows|数据|array|EventDto|
|&emsp;&emsp;&emsp;&emsp;displayName|事件显示名|string||
|&emsp;&emsp;&emsp;&emsp;eventName|事件名|string||
|&emsp;&emsp;&emsp;&emsp;eventType|事件类型  1:预置事件  0:自定义事件|integer||
|&emsp;&emsp;&emsp;&emsp;eventsNum|昨日事件量|integer||
|&emsp;&emsp;&emsp;&emsp;id|事件id|integer||
|&emsp;&emsp;&emsp;&emsp;ps|备注|string||
|&emsp;&emsp;&emsp;&emsp;showType|显示状态|integer||
|&emsp;&emsp;total|总条数|integer(int64)||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"rows": [
			{
				"displayName": "",
				"eventName": "",
				"eventType": 0,
				"eventsNum": 0,
				"id": 0,
				"ps": "",
				"showType": 0
			}
		],
		"total": 100
	},
	"msg": ""
}
```


## loadFilterProper


**接口地址**:`/reportservice/event/filtProperties`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|项目id|query|true|integer(int32)||
|eventCodes|事件Code列表|query|false|ref||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«FilterProperties»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|FilterProperties|FilterProperties|
|&emsp;&emsp;eventAttrs||array|EventAttr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;&emsp;&emsp;attrName|显示名称|string||
|&emsp;&emsp;&emsp;&emsp;dataType|dataType,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;hasDict|hasDict|integer||
|&emsp;&emsp;&emsp;&emsp;systemCode|doris字段|string||
|&emsp;&emsp;userAttrs||array|UserAttr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;&emsp;&emsp;attrName|显示名称|string||
|&emsp;&emsp;&emsp;&emsp;createTime||string||
|&emsp;&emsp;&emsp;&emsp;createUser||string||
|&emsp;&emsp;&emsp;&emsp;dataType|dataType,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;dbType|dbType|string||
|&emsp;&emsp;&emsp;&emsp;display|display|integer||
|&emsp;&emsp;&emsp;&emsp;hasDict|hasDict|integer||
|&emsp;&emsp;&emsp;&emsp;id||integer||
|&emsp;&emsp;&emsp;&emsp;isCommon||integer||
|&emsp;&emsp;&emsp;&emsp;isNull|isNull|integer||
|&emsp;&emsp;&emsp;&emsp;projectId|应用id|integer||
|&emsp;&emsp;&emsp;&emsp;sorting|sorting|integer||
|&emsp;&emsp;&emsp;&emsp;status|status|integer||
|&emsp;&emsp;&emsp;&emsp;systemCode||string||
|&emsp;&emsp;&emsp;&emsp;updateTime||string||
|&emsp;&emsp;&emsp;&emsp;updateUser||string||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"eventAttrs": [
			{
				"attrCode": "",
				"attrName": "",
				"dataType": "",
				"hasDict": 0,
				"systemCode": "user_id"
			}
		],
		"userAttrs": [
			{
				"attrCode": "sex",
				"attrName": "性别",
				"createTime": "",
				"createUser": "",
				"dataType": "bool",
				"dbType": "number",
				"display": 1,
				"hasDict": 0,
				"id": 0,
				"isCommon": 0,
				"isNull": 0,
				"projectId": 101,
				"sorting": 10,
				"status": 1,
				"systemCode": "",
				"updateTime": "",
				"updateUser": ""
			}
		]
	},
	"msg": ""
}
```


## getEventOrAttrFunction


**接口地址**:`/reportservice/event/function`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|dataType|dataType,可用值:string,dictionary,number,bool,datetime|query|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«List«string»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|array||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [],
	"msg": ""
}
```


## AttrRelatedEvents


**接口地址**:`/reportservice/event/particular-event/{projectId}/{attrCode}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>查询属性关联的事件</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|path|true|integer(int32)||
|attrCode|属性code|path|false|string||
|eventName|事件名或者code|query|false|string||
|eventType|事件类型|query|false|integer(int32)||
|display|是否显示|query|false|integer(int32)||
|current|当前页|query|false|integer(int32)||
|size|每页大小|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«PageResultDto«EventDto»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|PageResultDto«EventDto»|PageResultDto«EventDto»|
|&emsp;&emsp;rows|数据|array|EventDto|
|&emsp;&emsp;&emsp;&emsp;displayName|事件显示名|string||
|&emsp;&emsp;&emsp;&emsp;eventName|事件名|string||
|&emsp;&emsp;&emsp;&emsp;eventType|事件类型  1:预置事件  0:自定义事件|integer||
|&emsp;&emsp;&emsp;&emsp;eventsNum|昨日事件量|integer||
|&emsp;&emsp;&emsp;&emsp;id|事件id|integer||
|&emsp;&emsp;&emsp;&emsp;ps|备注|string||
|&emsp;&emsp;&emsp;&emsp;showType|显示状态|integer||
|&emsp;&emsp;total|总条数|integer(int64)||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"rows": [
			{
				"displayName": "",
				"eventName": "",
				"eventType": 0,
				"eventsNum": 0,
				"id": 0,
				"ps": "",
				"showType": 0
			}
		],
		"total": 100
	},
	"msg": ""
}
```


## eventAnalysis


**接口地址**:`/reportservice/event/report`


**请求方式**:`POST`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "byFields": "age",
  "current": 1,
  "filter": {
    "conditions": [
      {
        "attr": {
          "attrCode": "event_time",
          "attrName": "事件触发时间",
          "attrType": "1",
          "dataType": "number",
          "intervals": "[0,100,300]"
        },
        "conditions": "[]",
        "functionAlias": "date",
        "functionCode": "EQUAL(\"等于\", \"equal\")",
        "params": "[]",
        "relation": "and",
        "renderIndex": 2
      }
    ],
    "relation": ""
  },
  "fromDate": "2021-08-20T09:26:03.281Z",
  "indicators": [
    {
      "aggregatorAlias": "",
      "aggregatorCode": "",
      "eventAlias": "logout",
      "eventAttr": {
        "attrCode": "event_time",
        "attrName": "事件触发时间",
        "attrType": "1",
        "dataType": "number",
        "intervals": "[0,100,300]"
      },
      "eventCode": "login",
      "filter": {
        "conditions": [
          {
            "attr": {
              "attrCode": "event_time",
              "attrName": "事件触发时间",
              "attrType": "1",
              "dataType": "number",
              "intervals": "[0,100,300]"
            },
            "conditions": "[]",
            "functionAlias": "date",
            "functionCode": "EQUAL(\"等于\", \"equal\")",
            "params": "[]",
            "relation": "and",
            "renderIndex": 2
          }
        ],
        "relation": ""
      },
      "indicatorAlias": "年龄",
      "indicatorName": "age"
    }
  ],
  "projectId": 101,
  "size": 10,
  "toDate": "2021-08-19T09:26:03.281Z",
  "unit": "week"
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|request|分析请求公用类|body|true|EventAnalysisRequest|EventAnalysisRequest|
|&emsp;&emsp;byFields|分组字段||false|array|Attr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;current|当前页号||false|integer(int32)||
|&emsp;&emsp;filter|filter||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;fromDate|开始时间||true|string(date-time)||
|&emsp;&emsp;indicators|指标||true|array|Indicator|
|&emsp;&emsp;&emsp;&emsp;aggregatorAlias|||false|string||
|&emsp;&emsp;&emsp;&emsp;aggregatorCode|可用值:COUNT,COUNT_USER,AVG_USER,USER_ID_LIST_DISTINCT,COUNT_DISTINCT,SUM,AVG,MAX,MIN,PER_CAPITA,IS_TRUE,IS_FALSE,IS_NULL,NOT_NULL||false|string||
|&emsp;&emsp;&emsp;&emsp;eventAlias|需要分析的事件别名||true|string||
|&emsp;&emsp;&emsp;&emsp;eventAttr|事件属性||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;eventCode|需要分析的事件code||true|string||
|&emsp;&emsp;&emsp;&emsp;filter|||false|Filter|Filter|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attr|筛选条件的字段||false|Attr|Attr|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrName|属性名||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间||true|array|number|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;conditions|二次筛选条件||false|array|Condition|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionAlias|筛选字段的别名||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;functionCode|筛选字段的判断类型,可用值:EQUAL,NOT_EQUAL,IN,NOT_IN,NOT_NULL,IS_NULL,IS_EMPTY,NOT_EMPTY,REGEXP,NOT_REGEXP,LESS_THEN,GREATER_THAN,INTERVAL,IS_TRUE,IS_FALSE,ABSOLUTE_TIME,TIME_INTERVAL,RELATIVE_CURRENT_TIME,RELATIVE_WITHIN,RELATIVE_BETWEEN,RELATIVE_EVENT_TIME||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;params|筛选条件的判断值||false|array|string|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|二次筛选条件之间的关系,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;renderIndex|渲染索引||false|integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;relation|and|or ,可用值:and,or||false|string||
|&emsp;&emsp;&emsp;&emsp;indicatorAlias|指标别名||true|string||
|&emsp;&emsp;&emsp;&emsp;indicatorName|指标显示名称||true|string||
|&emsp;&emsp;projectId|应用id||true|integer(int32)||
|&emsp;&emsp;size|每页大小||false|integer(int32)||
|&emsp;&emsp;toDate|终止时间||true|string(date-time)||
|&emsp;&emsp;unit|分割单位,可用值:MONTH,WEEK,DAY,HOUR,MINUTE||true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«DetailResult»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|DetailResult|DetailResult|
|&emsp;&emsp;byFields|分组字段|array|Attr|
|&emsp;&emsp;&emsp;&emsp;attrCode|属性code|string||
|&emsp;&emsp;&emsp;&emsp;attrName|属性名|string||
|&emsp;&emsp;&emsp;&emsp;attrType|属性类型,可用值:user,event|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;intervals|对于数值类型的分组字段指定区间|array|number|
|&emsp;&emsp;indicatorNames|指标名称|array|string|
|&emsp;&emsp;rows|数据|array|Row|
|&emsp;&emsp;&emsp;&emsp;byValues|分组字段对应的值|array|object|
|&emsp;&emsp;&emsp;&emsp;values|该分组每个时间段的值,里面的double数组是有几个指标，数组长度就有多少|array|array|
|&emsp;&emsp;series|横坐标|array|integer|
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"byFields": "age",
		"indicatorNames": "name",
		"rows": [
			{
				"byValues": [],
				"values": []
			}
		],
		"series": "100"
	},
	"msg": ""
}
```


## event


**接口地址**:`/reportservice/event/{projectId}/{eventId}`


**请求方式**:`PATCH`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>更新事件名</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|path|true|ref||
|eventId|事件id|path|true|ref||
|displayName|显示名|query|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|204|No Content||
|401|Unauthorized||
|403|Forbidden||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


# 事件属性管理


## updateEventAttr


**接口地址**:`/reportservice/event-attr`


**请求方式**:`PUT`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|eventAttr|事件属性实体|body|true|EventAttr|EventAttr|
|&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;attrName|显示名称||true|string||
|&emsp;&emsp;dataType|dataType,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;hasDict|hasDict||true|integer(int32)||
|&emsp;&emsp;systemCode|doris字段||false|string||
|EventAttr|事件属性实体|body|true|EventAttr|EventAttr|
|&emsp;&emsp;attrCode|属性code||true|string||
|&emsp;&emsp;attrName|显示名称||true|string||
|&emsp;&emsp;dataType|dataType,可用值:string,dictionary,number,bool,datetime||true|string||
|&emsp;&emsp;hasDict|hasDict||true|integer(int32)||
|&emsp;&emsp;systemCode|doris字段||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## selectEventMeta


**接口地址**:`/reportservice/event-attr/event-attrs/{projectId}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>分页查询事件属性</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|path|true|integer(int32)||
|attrName|属性名|query|false|string||
|dataType|数据类型|query|false|string||
|display|是否显示|query|false|integer(int32)||
|attrType|属性类型|query|false|integer(int32)||
|current|当前页|query|false|integer(int32)||
|size|每页大小|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«PageResultDto«MetaDto»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|PageResultDto«MetaDto»|PageResultDto«MetaDto»|
|&emsp;&emsp;rows|数据|array|MetaDto|
|&emsp;&emsp;&emsp;&emsp;attrName|属性名|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;dimension|维度表|string||
|&emsp;&emsp;&emsp;&emsp;displayName|属性显示名|string||
|&emsp;&emsp;&emsp;&emsp;eventType|属性类型|integer||
|&emsp;&emsp;&emsp;&emsp;id|属性id|integer||
|&emsp;&emsp;&emsp;&emsp;showType|显示状态|integer||
|&emsp;&emsp;&emsp;&emsp;systemCode|属性表字段|string||
|&emsp;&emsp;&emsp;&emsp;unit|单位|string||
|&emsp;&emsp;total|总条数|integer(int64)||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"rows": [
			{
				"attrName": "",
				"dataType": "",
				"dimension": "",
				"displayName": "",
				"eventType": 0,
				"id": 0,
				"showType": 0,
				"systemCode": "",
				"unit": ""
			}
		],
		"total": 100
	},
	"msg": ""
}
```


## eventRelatedAttrs


**接口地址**:`/reportservice/event-attr/particular-attr/{projectId}/{eventCode}`


**请求方式**:`GET`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>查询事件关联的属性</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|path|true|integer(int32)||
|eventCode|事件code|path|false|string||
|attrName|属性名|query|false|string||
|dataType|数据类型|query|false|string||
|display|是否显示|query|false|integer(int32)||
|attrType|属性类型|query|false|string||
|current|当前页|query|false|integer(int32)||
|size|每页大小|query|false|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result«PageResultDto«MetaDto»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|PageResultDto«MetaDto»|PageResultDto«MetaDto»|
|&emsp;&emsp;rows|数据|array|MetaDto|
|&emsp;&emsp;&emsp;&emsp;attrName|属性名|string||
|&emsp;&emsp;&emsp;&emsp;dataType|数据类型,可用值:string,dictionary,number,bool,datetime|string||
|&emsp;&emsp;&emsp;&emsp;dimension|维度表|string||
|&emsp;&emsp;&emsp;&emsp;displayName|属性显示名|string||
|&emsp;&emsp;&emsp;&emsp;eventType|属性类型|integer||
|&emsp;&emsp;&emsp;&emsp;id|属性id|integer||
|&emsp;&emsp;&emsp;&emsp;showType|显示状态|integer||
|&emsp;&emsp;&emsp;&emsp;systemCode|属性表字段|string||
|&emsp;&emsp;&emsp;&emsp;unit|单位|string||
|&emsp;&emsp;total|总条数|integer(int64)||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"rows": [
			{
				"attrName": "",
				"dataType": "",
				"dimension": "",
				"displayName": "",
				"eventType": 0,
				"id": 0,
				"showType": 0,
				"systemCode": "",
				"unit": ""
			}
		],
		"total": 100
	},
	"msg": ""
}
```


## updateDisplayName


**接口地址**:`/reportservice/event-attr/{projectId}/{attrId}`


**请求方式**:`PATCH`


**请求数据类型**:`application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>修改事件属性显示名</p>



**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|projectId|产品id|path|true|ref||
|attrId|属性id|path|true|ref||
|displayName|显示名|query|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|Result|
|204|No Content||
|401|Unauthorized||
|403|Forbidden||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code|状态码|integer(int32)|integer(int32)|
|data|具体数据|object||
|msg|错误信息|string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```