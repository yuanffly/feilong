module.exports = {
	root: true,
	env: {
		node: true
	},
	extends: ['plugin:vue/essential', 'eslint:recommended', '@vue/typescript/recommended', '@vue/prettier', '@vue/prettier/@typescript-eslint'],
	parserOptions: { //支持的解析js版本
		ecmaVersion: 2020
	},
	rules: {
		'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
		'semi': ['error', 'never'], // 行尾不使用分号
		"no-underscore-dangle": 0, // 不允许下划线作为变量名之一
		'generator-star-spacing': 'off', // allow async-await
		'@typescript-eslint/no-var-requires': 0,
		'@typescript-eslint/no-explicit-any': 'off',
		"@typescript-eslint/explicit-module-boundary-types": "off", //input.vuets每个函数都要显式声明返回值
		'prettier/prettier': 'off',
		'prefer-const': 'off',
		'space-before-function-paren': 0,
		"no-irregular-whitespace": "off", //这禁止掉空格报错检查
		'max-len': [
			'error',
			{
				code: 300,
				ignoreUrls: true
			}
		]
	}
}

// "off"   或0 - 关闭规则
// "warn"  或1 - 将该规则作为警告打开（不影响退出代码）
// "error" 或2 - 将规则作为错误打开（触发时退出代码为1）
